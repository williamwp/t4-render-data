==PROF== Disconnected from process 18810
[18810] a.out@127.0.0.1
  kernel(unsigned char *), 2023-Mar-01 02:41:19, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Kbyte                         300.80
    dram__bytes_read.sum.per_second                                           Mbyte/second                         639.37
    dram__bytes_write.sum                                                            Mbyte                           2.94
    dram__bytes_write.sum.per_second                                          Gbyte/second                           6.26
    dram__sectors_read.sum                                                          sector                          9,400
    dram__sectors_write.sum                                                         sector                         91,964
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           2.16
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         583.57
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           0.60
    l1tex__lsu_writeback_active.sum                                                  cycle                         65,536
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                    byte/second                              0
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                          16.78
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Gbyte/second                          35.66
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                     byte/second                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                        131,072
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                              0
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          75.49
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                              0
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                 sector/second                              0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                     byte                              0
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                           4.11
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          97.01
    lts__t_sector_op_read_hit_rate.pct                                                   %                          14.61
    lts__t_sector_op_write_hit_rate.pct                                                  %                          96.98
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           1.08
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              7
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                          14.88
    lts__t_sectors_op_read.sum                                                      sector                          1,821
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                           3.87
    lts__t_sectors_op_write.sum                                                     sector                        136,918
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                         291.03
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                              0
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                        sector/second                              0
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.03
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.09
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          98.82
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                          12.85
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          94.44
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                         946.38
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          98.99
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.71
    smsp__inst_executed.sum                                                           inst                     31,011,055
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                              0
    smsp__inst_executed_op_global_st.sum                                              inst                        131,072
    smsp__inst_executed_op_local_ld.sum                                               inst                              0
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                      5,333,989
    smsp__inst_executed_pipe_cbu.sum                                                  inst                      4,123,781
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           0.90
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.71
    smsp__inst_issued.sum                                                             inst                     31,015,544
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          71.33
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                    140,701,144
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                    130,215,384
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                      8,041,484
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                    133,797,115
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                    356,671,107
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                     31,235,728
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                     90,076,822
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                     98,659,757
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          99.63
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              31.88
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.08
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                         261.32
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         852.87
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          93.48
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a

