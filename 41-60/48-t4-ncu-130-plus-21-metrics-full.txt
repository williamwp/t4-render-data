==PROF== Connected to process 18734 (/home/test/tools/backup6/GPU-rendering-programs/48-Ray-tracer-texture-sample2/a.out)
created the scene successfully 
this one worked 
==PROF== Profiling "render" - 1: 0%....50%....100% - 13 passes
kernel worked
shits successful 
took 8.75898 seconds.
==PROF== Disconnected from process 18734
[18734] a.out@127.0.0.1
  render(RGBType *, int, int, double, int, int, double, double, double, int, sphere *, int, plane *, int, light *, int, camera), 2023-Mar-01 02:39:44, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Mbyte                          12.37
    dram__bytes_read.sum.per_second                                           Mbyte/second                          21.64
    dram__bytes_write.sum                                                            Mbyte                         345.33
    dram__bytes_write.sum.per_second                                          Mbyte/second                         604.31
    dram__sectors_read.sum                                                          sector                        386,432
    dram__sectors_write.sum                                                         sector                     10,791,687
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.20
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         584.98
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           0.93
    l1tex__lsu_writeback_active.sum                                                  cycle                    124,688,978
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           1.11
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                         150.54
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                         263.44
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                          18.74
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                     19,794,867
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                        196,020
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                     84,499,008
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                     84,166,386
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          97.37
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                     19,794,867
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                    334,655,378
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                    333,387,384
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/usecond                         583.40
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                         645.89
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                          50.10
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          99.88
    lts__t_sector_op_read_hit_rate.pct                                                   %                          97.62
    lts__t_sector_op_write_hit_rate.pct                                                  %                          99.94
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           2.24
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                             42
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                   sector/second                          73.50
    lts__t_sectors_op_read.sum                                                      sector                     14,568,597
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                          25.49
    lts__t_sectors_op_write.sum                                                     sector                    334,972,877
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                         586.18
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                      7,410,450
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                          12.97
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.00
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.00
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          99.93
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           0.47
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          24.80
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                      23,664.59
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          99.60
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.03
    smsp__inst_executed.sum                                                           inst                  1,557,697,925
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                     19,230,129
    smsp__inst_executed_op_global_st.sum                                              inst                        196,020
    smsp__inst_executed_op_local_ld.sum                                               inst                     83,981,718
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                        654,368
    smsp__inst_executed_pipe_cbu.sum                                                  inst                     89,510,224
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                          21.65
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           0.71
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.03
    smsp__inst_issued.sum                                                             inst                  1,557,751,501
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                           2.92
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                  2,961,285,197
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                  7,956,011,434
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                 10,946,345,056
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                  2,359,576,091
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                    194,439,283
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                  1,664,430,989
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                 22,322,923,385
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                 10,259,443,460
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                  5,881,206,448
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                  2,525,656,158
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          97.99
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              31.36
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.00
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                           4.75
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         854.97
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          24.78
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a

