Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-7476-d193-c4f4-48a3.qdrep"
Exporting 3442 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-7476-d193-c4f4-48a3.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls     Average      Minimum       Maximum         StdDev               Name         
 -------  ---------------  ---------  -------------  ----------  -------------  ---------------  ---------------------
    98.2   26,079,359,621         28  931,405,700.8       1,084  3,756,896,122  1,642,071,270.9  cudaStreamSynchronize
     1.3      345,494,119          6   57,582,353.2       2,166    345,329,219    140,966,604.4  cudaMalloc           
     0.3       68,819,510          6   11,469,918.3         341     68,591,214     27,983,721.8  cudaFree             
     0.2       61,058,014          2   30,529,007.0  29,978,633     31,079,381        778,346.4  cudaDeviceReset      
     0.0          520,548          1      520,548.0     520,548        520,548              0.0  cudaHostAlloc        
     0.0          268,478         42        6,392.3       2,124         19,370          5,820.9  cudaLaunchKernel     
     0.0           63,519         14        4,537.1       2,642          8,879          1,992.5  cudaMemcpyAsync      
     0.0            3,560          2        1,780.0       1,413          2,147            519.0  cuCtxSynchronize     
     0.0            1,077          1        1,077.0       1,077          1,077              0.0  cudaFreeHost         



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances      Average         Minimum        Maximum        StdDev                                                      Name                                                
 -------  ---------------  ---------  ---------------  -------------  -------------  -------------  ----------------------------------------------------------------------------------------------------
   100.0   26,068,518,208         14  1,862,037,014.9  1,416,495,900  2,340,233,979  444,571,936.8  ker_pixel_shader(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int…
     0.0        9,719,180         14        694,227.1        484,466      1,323,258      280,448.9  ker_mesh_shader(float, quat, unsigned int, unsigned int, triangle_t*)                               
     0.0           49,472         14          3,533.7          2,240         13,664        3,015.4  ker_lights_init(light_t*)                                                                           



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum   StdDev       Operation     
 -------  ---------------  ----------  --------  -------  -------  --------  ------------------
   100.0        1,116,543          14  79,753.1   60,830   98,590  19,519.2  [CUDA memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

   Total     Operations   Average   Minimum   Maximum   StdDev       Operation     
 ----------  ----------  ---------  -------  ---------  -------  ------------------
 14,175.000          14  1,012.500  772.500  1,252.500  249.060  [CUDA memcpy DtoH]



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    99.6   26,553,790,953        308  86,213,607.0    2,870  100,152,802  34,016,592.5  poll          
     0.4       99,954,826      1,837      54,412.0    1,006    8,409,332     326,030.2  ioctl         
     0.0        6,224,684        229      27,182.0    1,489      714,550      76,479.0  mmap64        
     0.0        2,575,427         50      51,508.5    1,106      236,921      72,300.9  mmap          
     0.0        1,725,911         40      43,147.8    3,363      155,809      33,186.6  sem_timedwait 
     0.0        1,187,096         13      91,315.1    1,346      179,205      86,139.7  open          
     0.0          991,617        163       6,083.5    1,871       12,448       1,389.7  open64        
     0.0          646,577         30      21,552.6    1,016      548,273      99,547.1  fopen         
     0.0          485,023         59       8,220.7    1,059      126,928      22,890.0  munmap        
     0.0          153,453          4      38,363.3    1,232      112,998      52,459.3  fwrite        
     0.0          122,172          2      61,086.0   59,189       62,983       2,682.8  pthread_join  
     0.0           83,342         36       2,315.1    1,022        4,479         891.4  write         
     0.0           71,560          5      14,312.0   11,219       18,814       2,996.4  pthread_create
     0.0           66,908          8       8,363.5    1,278       33,168      10,849.0  fread         
     0.0           24,968          1      24,968.0   24,968       24,968           0.0  fgets         
     0.0           13,713          8       1,714.1    1,519        2,000         137.0  fflush        
     0.0           10,277          5       2,055.4    1,322        3,085         745.1  fclose        
     0.0            8,699          7       1,242.7    1,127        1,490         126.7  ftruncate     
     0.0            6,181          2       3,090.5    2,813        3,368         392.4  pipe2         
     0.0            6,001          3       2,000.3    1,849        2,201         181.1  fcntl         
     0.0            5,923          2       2,961.5    2,077        3,846       1,250.9  fgetc         
     0.0            3,483          1       3,483.0    3,483        3,483           0.0  connect       
     0.0            3,302          2       1,651.0    1,282        2,020         521.8  socket        
     0.0            1,106          1       1,106.0    1,106        1,106           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/44-Evert-cuda-sample2/report4.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/44-Evert-cuda-sample2/report4.sqlite"
