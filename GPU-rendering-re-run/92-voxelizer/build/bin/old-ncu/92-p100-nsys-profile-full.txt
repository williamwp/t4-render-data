
Importing [==================================================100%]
Saving report to file "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/92-voxelizer/build/bin/report3.qdrep"
Report file saved.
Please discard the qdstrm file and use the qdrep file instead.

Removed /home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/92-voxelizer/build/bin/report3.qdstrm as it was successfully imported.
Please use the qdrep file instead.

Exporting the qdrep file to SQLite database using /usr/local/cuda-10.2/nsight-systems-2019.5.2/host-linux-x64/nsys-exporter.

Exporting 2090 events:

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/92-voxelizer/build/bin/report3.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.1     20233978122         212      95443293.0           25333       100202351  poll                                                                            
    0.8       166038304         555        299168.1            1016        78894837  ioctl                                                                           
    0.0         8221980         335         24543.2           20000           42473  writev                                                                          
    0.0         2315262          58         39918.3            1564          893452  mmap                                                                            
    0.0         1503984          30         50132.8            2083         1353989  fopen                                                                           
    0.0         1365363          59         23141.7            6055           44201  open64                                                                          
    0.0          560540           2        280270.0           13669          546871  fopen64                                                                         
    0.0          476398           9         52933.1           11682           95054  sem_timedwait                                                                   
    0.0          424201          26         16315.4            1199          371068  fclose                                                                          
    0.0          295896           7         42270.9            3953          220286  fread                                                                           
    0.0          250908           3         83636.0           78911           92850  fgets                                                                           
    0.0          114437          16          7152.3            1465           23850  write                                                                           
    0.0          104519           2         52259.5           42812           61707  pthread_create                                                                  
    0.0           59081          16          3692.6            1600           13408  read                                                                            
    0.0           43976           4         10994.0            1094           21332  fflush                                                                          
    0.0           41901           8          5237.6            2814           12931  munmap                                                                          
    0.0           38436           4          9609.0            4578           12489  open                                                                            
    0.0           17620          10          1762.0            1013            4729  fcntl                                                                           
    0.0           17321           2          8660.5            5365           11956  socket                                                                          
    0.0           14507           1         14507.0           14507           14507  connect                                                                         
    0.0           12855           3          4285.0            2183            6683  fwrite                                                                          
    0.0            9818           1          9818.0            9818            9818  pipe2                                                                           
    0.0            6685           2          3342.5            1307            5378  pthread_mutex_trylock                                                           
    0.0            3043           1          3043.0            3043            3043  bind                                                                            
    0.0            1089           1          1089.0            1089            1089  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)


