wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/92-voxelizer/build/bin$ sudo nvprof --print-gpu-trace  ./voxelizer -r 640 ../../data/sphere/sphere.obj   ../../data/sphere/sphere_voxelized

Loading Mesh
Num Triangles: 960
Voxelizing in the GPU, this might take a while.
==45157== NVPROF is profiling process 45157, command: ./voxelizer -r 640 ../../data/sphere/sphere.obj ../../data/sphere/sphere_voxelized
Summary: sphere.obj (960 triangles) @ 640x640x640, 1 sample in: 4.63342 seconds
Saving Results.
==45157== Profiling application: ./voxelizer -r 640 ../../data/sphere/sphere.obj ../../data/sphere/sphere_voxelized
==45157== Profiling result:
   Start  Duration            Grid Size      Block Size     Regs*    SSMem*    DSMem*      Size  Throughput  SrcMemType  DstMemType           Device   Context    Stream  Name
544.87ms  37.493ms                    -               -         -         -         -  250.00MB  6.5117GB/s    Pageable      Device  Tesla P100-PCIE         1         7  [CUDA memcpy HtoD]
582.66ms  4.8960us                    -               -         -         -         -  33.750KB  6.5740GB/s    Pageable      Device  Tesla P100-PCIE         1         7  [CUDA memcpy HtoD]
582.71ms  3.82344s           (80 80 80)         (8 8 8)        33        0B        0B         -           -           -           -  Tesla P100-PCIE         1         7  voxelize_kernel(bool*, CompFab::TriangleStruct*, int, float, float3, int, int, int, bool) [222]
4.40638s  322.20ms                    -               -         -         -         -  250.00MB  775.91MB/s      Device    Pageable  Tesla P100-PCIE         1         7  [CUDA memcpy DtoH]

Regs: Number of registers used per CUDA thread. This number includes registers used internally by the CUDA driver and/or tools and can be more than what the compiler shows.
SSMem: Static shared memory allocated per CUDA block.
DSMem: Dynamic shared memory allocated per CUDA block.
SrcMemType: The type of source memory accessed by memory operation/copy
DstMemType: The type of destination memory accessed by memory operation/copy
