
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-6e79-9cd1-b8d2-7c60.qdrep"
Exporting 7043 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-6e79-9cd1-b8d2-7c60.sqlite


Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum         Name     
 -------  ---------------  ---------  ------------  -------  -----------  --------------
    91.1   53,908,938,611      2,707  19,914,643.0   25,084  100,180,041  poll          
     8.5    5,044,463,759      2,409   2,094,007.4   13,857   21,081,609  sem_timedwait 
     0.4      220,192,644        803     274,212.5    1,149   81,570,361  ioctl         
     0.0        3,557,432        103      34,538.2    2,055      728,495  mmap          
     0.0        1,812,106         98      18,490.9    5,969       38,656  open64        
     0.0        1,716,210         44      39,004.8    1,771    1,495,010  fopen         
     0.0          623,487         39      15,986.8    1,300       87,551  fread         
     0.0          252,256          3      84,085.3   78,271       94,629  fgets         
     0.0          182,305          4      45,576.3   43,057       47,914  pthread_create
     0.0           66,864         34       1,966.6    1,021        4,008  fclose        
     0.0           48,398         10       4,839.8    2,806        7,269  write         
     0.0           39,005          5       7,801.0    4,259       12,578  open          
     0.0           37,173          8       4,646.6    2,676        6,729  munmap        
     0.0           34,213         12       2,851.1    1,587        4,326  read          
     0.0           31,723         22       1,442.0    1,000        4,723  fcntl         
     0.0           17,683          3       5,894.3    1,167       11,518  fgetc         
     0.0           10,760          2       5,380.0    5,220        5,540  socket        
     0.0            9,537          1       9,537.0    9,537        9,537  pipe2         
     0.0            8,913          1       8,913.0    8,913        8,913  connect       
     0.0            2,776          1       2,776.0    2,776        2,776  bind          
     0.0            1,147          1       1,147.0    1,147        1,147  listen        

Report file moved to "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report1.qdrep"
Report file moved to "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report1.sqlite"
