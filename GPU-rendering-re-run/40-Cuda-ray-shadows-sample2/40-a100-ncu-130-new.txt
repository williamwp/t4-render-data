==PROF== Connected to process 95220 (/home/wangpeng/t4-render-data/GPU-rendering-re-run/92-voxelizer/build/bin/voxelizer)
==PROF== Profiling "voxelize_kernel" - 1: 0%....50%....100% - 13 passes
Summary: bunny.obj (69664 triangles) @ 64x64x64, 1 sample in: 24.4419 seconds
Saving Results.
==PROF== Disconnected from process 95220
[95220] voxelizer@127.0.0.1
  voxelize_kernel(bool*, CompFab::TriangleStruct*, int, float, float3, int, int, int, bool), 2023-Mar-24 16:01:07, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Mbyte                           2.77
    dram__bytes_read.sum.per_second                                           Mbyte/second                          22.48
    dram__bytes_write.sum                                                             byte                              0
    dram__bytes_write.sum.per_second                                           byte/second                              0
    dram__sectors_read.sum                                                          sector                         86,688
    dram__sectors_write.sum                                                         sector                              0
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          52.71
    l1tex__lsu_writeback_active.sum                                                  cycle                  5,136,211,968
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Tbyte/second                           1.33
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                           1.05
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                           8.50
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                     byte/second                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                  5,136,187,392
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                          8,192
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                              0
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          99.25
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                  5,136,187,392
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                 sector/second                              0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Gbyte                           1.24
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                           1.05
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          99.85
    lts__t_sector_op_read_hit_rate.pct                                                   %                          99.81
    lts__t_sector_op_write_hit_rate.pct                                                  %                         100.37
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           0.26
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                             15
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                   sector/second                         121.58
    lts__t_sectors_op_read.sum                                                      sector                     38,798,506
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                         314.48
    lts__t_sectors_op_write.sum                                                     sector                         51,564
    lts__t_sectors_op_write.sum.per_second                                  sector/msecond                         417.95
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                     38,681,416
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                         313.53
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                          25.18
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          61.79
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                   3,573,646.10
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          95.54
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.75
    smsp__inst_executed.sum                                                           inst                 29,275,308,888
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                  5,136,187,392
    smsp__inst_executed_op_global_st.sum                                              inst                          8,192
    smsp__inst_executed_op_local_ld.sum                                               inst                              0
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                         24,576
    smsp__inst_executed_pipe_cbu.sum                                                  inst                  1,123,285,344
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                          11.79
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                          13.18
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.75
    smsp__inst_issued.sum                                                             inst                 29,275,341,063
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          75.11
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                 85,546,204,672
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                181,110,866,432
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                163,147,046,656
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                 72,553,716,992
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                452,692,394,496
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                 36,524,255,872
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                 99,257,679,872
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                164,358,258,688
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                 17,869,832,192
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          99.67
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              31.89
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.00
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                       1,477.92
