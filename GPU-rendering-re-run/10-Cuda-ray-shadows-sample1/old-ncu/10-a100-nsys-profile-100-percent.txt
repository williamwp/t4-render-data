wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1$ /home/chenjiuyi/usr_chenjiuyi/local/cuda-11.1/bin/nsys   profile  --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Time per frame : 20.3847 seconds
Time per frame : 21.1221 seconds
Time per frame : 22.6068 seconds
Time per frame : 20.5091 seconds
Time per frame : 20.2006 seconds
Time per frame : 20.0347 seconds
Time per frame : 20.4544 seconds
Time per frame : 19.3699 seconds
Time per frame : 19.2089 seconds
Time per frame : 20.2115 seconds
Time per frame : 21.1222 seconds
Time per frame : 22.0366 seconds
Time per frame : 23.3501 seconds
Time per frame : 21.952 seconds
Time per frame : 22.442 seconds
Time per frame : 21.836 seconds
Time per frame : 24.3254 seconds
Time per frame : 23.3237 seconds
Time per frame : 24.0821 seconds
Time per frame : 23.016 seconds
Time per frame : 24.3751 seconds
Time per frame : 22.479 seconds
Time per frame : 23.3889 seconds
Time per frame : 22.4873 seconds
Time per frame : 20.2917 seconds
Time per frame : 20.0672 seconds
Time per frame : 20.715 seconds
Time per frame : 21.5855 seconds
Time per frame : 23.8603 seconds
Time per frame : 26.2882 seconds
Time per frame : 25.3445 seconds
Time per frame : 24.1758 seconds
Time per frame : 24.2028 seconds
Time per frame : 23.1812 seconds
Time per frame : 24.2049 seconds
Time per frame : 24.1927 seconds
Time per frame : 24.3285 seconds
Time per frame : 23.727 seconds
Time per frame : 23.8255 seconds
Time per frame : 24.9698 seconds
Time per frame : 24.4294 seconds
Time per frame : 25.3154 seconds
Time per frame : 26.8747 seconds
Time per frame : 26.5815 seconds
Time per frame : 26.2725 seconds
Time per frame : 25.2685 seconds
Time per frame : 24.8226 seconds
Time per frame : 26.2732 seconds
Time per frame : 23.1295 seconds
Time per frame : 23.7955 seconds
Time per frame : 22.8643 seconds
Time per frame : 25.2374 seconds
Time per frame : 24.4754 seconds
Time per frame : 25.2654 seconds
Time per frame : 24.8717 seconds
Time per frame : 23.4057 seconds
Time per frame : 25.0954 seconds
Time per frame : 23.2243 seconds
Time per frame : 24.1678 seconds
Time per frame : 24.8116 seconds
Time per frame : 24.1763 seconds
Time per frame : 22.1229 seconds
Time per frame : 23.2331 seconds
Time per frame : 22.1403 seconds
Time per frame : 21.8444 seconds
Time per frame : 20.9704 seconds
Time per frame : 20.4298 seconds
Time per frame : 21.9164 seconds
Time per frame : 22.6431 seconds
Time per frame : 22.099 seconds
Time per frame : 23.3648 seconds
Time per frame : 21.6406 seconds
Time per frame : 19.2187 seconds
Time per frame : 20.5355 seconds
Time per frame : 21.5453 seconds
Time per frame : 20.6596 seconds
Time per frame : 20.4468 seconds
Time per frame : 20.6707 seconds
Time per frame : 20.7941 seconds
Time per frame : 20.6592 seconds
Time per frame : 20.6442 seconds
Time per frame : 22.9831 seconds
Time per frame : 20.575 seconds
Time per frame : 20.7194 seconds
Time per frame : 21.2141 seconds
Time per frame : 22.1637 seconds
Time per frame : 21.2248 seconds
Time per frame : 20.8293 seconds
Time per frame : 19.7593 seconds
Time per frame : 21.3834 seconds
Time per frame : 21.0457 seconds
Time per frame : 21.1839 seconds
Time per frame : 20.1087 seconds
Time per frame : 20.0353 seconds
Time per frame : 19.6346 seconds
Time per frame : 19.0295 seconds
Time per frame : 20.0673 seconds
Time per frame : 20.7359 seconds
Time per frame : 22.1311 seconds
Time per frame : 21.9878 seconds
took 2247.51 seconds.
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-6d3f-f2bc-6f1f-603c.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-6d3f-f2bc-6f1f-603c.qdrep"
Exporting 432932 events: [================================================100%]

Exported successfully to
/tmp/nsys-report-6d3f-f2bc-6f1f-603c.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.9   2358658194478         202   11676525715.2           80782     27528366236  cudaDeviceSynchronize                                                           
    0.1      1353795054           5     270759010.8            7028      1309988774  cudaFree                                                                        
    0.0       924453449           1     924453449.0       924453449       924453449  cudaMallocManaged                                                               
    0.0        81109543           1      81109543.0        81109543        81109543  cudaDeviceReset                                                                 
    0.0        14650695         303         48352.1            6180         3943062  cudaLaunchKernel                                                                
    0.0         1119890           4        279972.5            3321          632318  cudaMalloc                                                                      
    0.0            9886           1          9886.0            9886            9886  cuCtxSynchronize                                                                




Generating CUDA Kernel Statistics...
CUDA Kernel Statistics (nanoseconds)

Time(%)      Total Time   Instances         Average         Minimum         Maximum  Name                                                                                                                                                                                                                                                                                                                                         
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------------------------------------------                                                                                                                                                                                                                         
  100.0   2354936028183         100   23549360281.8     20544957705     27528336653  render(vec3*, int, int, int, camera**, hitable**, curandStateXORWOW*)                                                                                                                                                                                                                                                                        
    0.0       417914857           1     417914857.0       417914857       417914857  render_init(int, int, curandStateXORWOW*)                                                                                                                                                                                                                                                                                                    
    0.0        41119827           1      41119827.0        41119827        41119827  create_world(hitable**, hitable**, curandStateXORWOW*)                                                                                                                                                                                                                                                                                       
    0.0        40759565           1      40759565.0        40759565        40759565  free_world(hitable**, hitable**)                                                                                                                                                                                                                                                                                                             
    0.0         8603729         100         86037.3           78561           88161  init_cam(camera**, unsigned int, unsigned int, unsigned int, unsigned int)                                                                                                                                                                                                                                                                   
    0.0         7677127         100         76771.3           65409           80354  delete_cam(camera**)                                                                                                                                                                                                                                                                                                                         



Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   54.4        49516511        9768          5069.3            1538           42286  [CUDA Unified Memory memcpy HtoD]                                               
   45.6        41520388        7300          5687.7            1370           44738  [CUDA Unified Memory memcpy DtoH]                                               


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
         844000.000            7300              115.616              4.000             1020.000  [CUDA Unified Memory memcpy DtoH]                                               
         835560.000            9768               85.541              4.000             1016.000  [CUDA Unified Memory memcpy HtoD]                                               




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   88.8   4313222879300      216367      19934753.8            1667       100264452  poll                                                                            
    8.3    404203124836      192779       2096717.6           11232        21106826  sem_timedwait                                                                   
    2.9    138656109818        3470      39958533.1            1006       934563396  ioctl                                                                           
    0.0         9193844         131         70182.0            1280         2141608  mmap                                                                            
    0.0         6017667         154         39075.8            1213         5007480  fopen                                                                           
    0.0         1680331          89         18880.1            5722           29810  open64                                                                          
    0.0          670639         100          6706.4            4860           13893  putc                                                                            
    0.0          564883          37         15267.1            1816          352737  munmap                                                                          
    0.0          263825          23         11470.7            1191           63238  fread                                                                           
    0.0          170436           4         42609.0           39898           44885  pthread_create                                                                  
    0.0          162064           3         54021.3           50709           58556  fgets                                                                           
    0.0          156098           1        156098.0          156098          156098  pthread_join                                                                    
    0.0           73477          21          3498.9            1117            5596  write                                                                           
    0.0           60825          35          1737.9            1042            8181  fwrite                                                                          
    0.0           47609          21          2267.1            1048            3058  read                                                                            
    0.0           42123          27          1560.1            1035            2444  fclose                                                                          
    0.0           31412           5          6282.4            3784            8486  open                                                                            
    0.0           13442           7          1920.3            1168            3368  fcntl                                                                           
    0.0            8556           2          4278.0            3913            4643  socket                                                                          
    0.0            8125           1          8125.0            8125            8125  connect                                                                         
    0.0            5031           1          5031.0            5031            5031  pipe2                                                                           
    0.0            2197           1          2197.0            2197            2197  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)
