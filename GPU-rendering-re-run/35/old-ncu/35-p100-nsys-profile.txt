Importing [==================================================100%]
Saving report to file "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/35-Ray-tracing-with-materials-sample2/report2.qdrep"
Report file saved.
Please discard the qdstrm file and use the qdrep file instead.

Removed /home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/35-Ray-tracing-with-materials-sample2/report2.qdstrm as it was successfully imported.
Please use the qdrep file instead.

Exporting the qdrep file to SQLite database using /usr/local/cuda-10.2/nsight-systems-2019.5.2/host-linux-x64/nsys-exporter.

Exporting 2075486 events:

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/35-Ray-tracing-with-materials-sample2/report2.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   59.5      5029915465          80      62873943.3            1580       100174582  poll                                                                            
   36.9      3124782636     2073621          1506.9            1138          719296  write                                                                           
    3.5       292875391         739        396313.1            1014        78266255  ioctl                                                                           
    0.1         6314328          91         69388.2            2034          952889  mmap                                                                            
    0.0         1545595          32         48299.8            1806         1348111  fopen                                                                           
    0.0         1276688          59         21638.8            5886           58664  open64                                                                          
    0.0         1000354          18         55575.2           13245          135719  sem_timedwait                                                                   
    0.0          503180          31         16231.6            2758          292898  munmap                                                                          
    0.0          296275           9         32919.4            1478          177281  fwrite                                                                          
    0.0          261337           3         87112.3           79218           91385  fgets                                                                           
    0.0          203448          26          7824.9            1430          147589  fclose                                                                          
    0.0          140908           1        140908.0          140908          140908  pthread_join                                                                    
    0.0           89023           2         44511.5           43909           45114  pthread_create                                                                  
    0.0           78236          23          3401.6            1590           14457  read                                                                            
    0.0           49157           1         49157.0           49157           49157  fopen64                                                                         
    0.0           33124           6          5520.7            2761           11669  fread                                                                           
    0.0           31686           4          7921.5            4352           13960  open                                                                            
    0.0           17630          10          1763.0            1038            4174  fcntl                                                                           
    0.0           14445           2          7222.5            3474           10971  fflush                                                                          
    0.0           12292           2          6146.0            5459            6833  socket                                                                          
    0.0            9349           1          9349.0            9349            9349  connect                                                                         
    0.0            8487           1          8487.0            8487            8487  pipe2                                                                           
    0.0            2271           1          2271.0            2271            2271  bind                                                                            
    0.0            2014           1          2014.0            2014            2014  putc                                                                            
    0.0            1151           1          1151.0            1151            1151  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)
