Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-8277-af01-7c69-9e8c.qdrep"
Exporting 5543 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-8277-af01-7c69-9e8c.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   56.6       253503730           3      84501243.3            2710       253497104  cudaMalloc                                                                      
   43.4       194204281           3      64734760.3           10869       194169315  cudaMemcpy                                                                      
    0.0           26114           1         26114.0           26114           26114  cudaLaunchKernel                                                                
    0.0            2541           3           847.0             341            1783  cudaFree                                                                        



CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   94.9     20313122252         701      28977349.9            1674       643359408  pthread_cond_wait                                                               
    2.6       553642271         116       4772778.2            1020       100186683  poll                                                                            
    1.3       286317753         943        303624.3            1323       128591928  ioctl                                                                           
    0.8       165249167          14      11803511.9           89465        16087800  pthread_mutex_lock                                                              
    0.2        52730964        1056         49934.6            1432          120092  pthread_barrier_wait                                                            
    0.0         6278771          38        165230.8            1244         6129303  fopen                                                                           
    0.0         6076495           6       1012749.2            4181         6025767  open                                                                            
    0.0         3385425          79         42853.5            1490         1350071  mmap                                                                            
    0.0         3243679          32        101365.0            3134          985898  pthread_join                                                                    
    0.0         2704412          71         38090.3            1107           66009  read                                                                            
    0.0         2275464          36         63207.3           29047          251112  pthread_create                                                                  
    0.0         2044988         682          2998.5            1044            9780  pthread_cond_signal                                                             
    0.0         1984957          95         20894.3            1359           40982  open64                                                                          
    0.0         1440785          10        144078.5           15352          830297  sem_timedwait                                                                   
    0.0          767329          54         14209.8            3767           29000  writev                                                                          
    0.0          338025          71          4760.9            1034           36216  recvmsg                                                                         
    0.0          185068          22          8412.2            1135           35552  recv                                                                            
    0.0          167083           3         55694.3           50932           64435  fgets                                                                           
    0.0           58550          12          4879.2            2059            8853  write                                                                           
    0.0           54483           1         54483.0           54483           54483  pthread_cond_broadcast                                                          
    0.0           49067           7          7009.6            1156           16382  fwrite                                                                          
    0.0           48952           2         24476.0            8929           40023  connect                                                                         
    0.0           41031          16          2564.4            2352            2950  prctl                                                                           
    0.0           38647          25          1545.9            1050            2886  fclose                                                                          
    0.0           27481           6          4580.2            2448           10680  fread                                                                           
    0.0           20725           5          4145.0            3475            4941  munmap                                                                          
    0.0           19869           3          6623.0            3810           10099  socket                                                                          
    0.0            9692           1          9692.0            9692            9692  shmget                                                                          
    0.0            8664           1          8664.0            8664            8664  shutdown                                                                        
    0.0            7048           1          7048.0            7048            7048  shmat                                                                           
    0.0            6662           1          6662.0            6662            6662  pipe2                                                                           
    0.0            5917           1          5917.0            5917            5917  mmap64                                                                          
    0.0            2522           1          2522.0            2522            2522  fcntl                                                                           
    0.0            2224           1          2224.0            2224            2224  fgets_unlocked                                                                  
    0.0            2135           1          2135.0            2135            2135  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/72-Mandelbrot-with-OpenGL-sample3/report2.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/72-Mandelbrot-with-OpenGL-sample3/report2.sqlite"

