wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/GPU-rendering-programs/70-Cuda-ray-shadows-sample3$ nsys profile --stats=true ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Time per frame : 22.6549 seconds
Time per frame : 23.1925 seconds
Time per frame : 23.1289 seconds
^CSignal 2 (SIGINT) was forwarded to the target application process group 57223

The target application terminated with signal 2 (SIGINT)
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-eaf8-e3cb-8b3b-c04c.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-eaf8-e3cb-8b3b-c04c.qdrep"
Exporting 17546 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-eaf8-e3cb-8b3b-c04c.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   91.5    154763847923        7774      19907878.6           32228       100246509  poll                                                                            
    8.4     14295415848        6923       2064916.3           14850        20886643  sem_timedwait                                                                   
    0.1       128002415        1051        121791.1            1044        22172354  ioctl                                                                           
    0.0         3403882          83         41010.6            1203         1215700  mmap                                                                            
    0.0         1567993          89         17617.9            5512           26317  open64                                                                          
    0.0          780511          57         13693.2            1134          529489  fopen                                                                           
    0.0          366669          24         15277.9            1101           87946  fread                                                                           
    0.0          201172           3         67057.3           53662           90509  fgets                                                                           
    0.0          178743           4         44685.7           42594           47691  pthread_create                                                                  
    0.0           49643          32          1551.3            1006            2542  fclose                                                                          
    0.0           45809          11          4164.5            2589            6180  write                                                                           
    0.0           35936           5          7187.2            4358           11358  open                                                                            
    0.0           31197           8          3899.6            2685            5510  munmap                                                                          
    0.0           28467           3          9489.0            5857           14565  putc                                                                            
    0.0           25693          11          2335.7            1158            3612  read                                                                            
    0.0           16112           8          2014.0            1114            3510  fcntl                                                                           
    0.0            9398           2          4699.0            4064            5334  socket                                                                          
    0.0            8426           3          2808.7            1053            6265  fwrite                                                                          
    0.0            8347           1          8347.0            8347            8347  pipe2                                                                           
    0.0            7901           1          7901.0            7901            7901  connect                                                                         
    0.0            2270           1          2270.0            2270            2270  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/70-Cuda-ray-shadows-sample3/report2.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/70-Cuda-ray-shadows-sample3/report2.sqlite"
