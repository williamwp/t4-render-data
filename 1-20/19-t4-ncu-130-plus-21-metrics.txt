==PROF== Profiling "trace2" - 1: 0%....50%....100% - 13 passes
took 0.380426 seconds.
==PROF== Disconnected from process 21360
[21360] a.out@127.0.0.1
  trace2(Vec3 *, Sphere *, int, unsigned int, unsigned int), 2023-Feb-28 16:38:21, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Mbyte                           6.98
    dram__bytes_read.sum.per_second                                           Gbyte/second                           4.28
    dram__bytes_write.sum                                                            Mbyte                           5.96
    dram__bytes_write.sum.per_second                                          Gbyte/second                           3.66
    dram__sectors_read.sum                                                          sector                        218,003
    dram__sectors_write.sum                                                         sector                        186,270
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           2.50
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         581.94
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           8.47
    l1tex__lsu_writeback_active.sum                                                  cycle                        828,256
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          14.88
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         774.88
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                         475.62
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                           4.53
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                        731,041
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                          1,689
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                         76,719
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                         76,719
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          73.48
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                        757,677
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                        230,580
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                        193,310
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/usecond                         118.65
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                         313.47
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Kbyte                         107.39
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          63.94
    lts__t_sector_op_read_hit_rate.pct                                                   %                          66.77
    lts__t_sector_op_write_hit_rate.pct                                                  %                          60.02
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           0.82
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              7
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                           4.30
    lts__t_sectors_op_read.sum                                                      sector                        159,022
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                          97.61
    lts__t_sectors_op_write.sum                                                     sector                        206,520
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                         126.76
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                        153,834
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                          94.42
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.01
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.03
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          25.77
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           1.07
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          21.93
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                      14,409.01
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          19.56
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.23
    smsp__inst_executed.sum                                                           inst                      6,743,417
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                        731,041
    smsp__inst_executed_op_global_st.sum                                              inst                          1,689
    smsp__inst_executed_op_local_ld.sum                                               inst                         76,719
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                            936
    smsp__inst_executed_pipe_cbu.sum                                                  inst                      1,383,604
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                           7.14
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           5.98
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.23
    smsp__inst_issued.sum                                                             inst                      6,752,591
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          22.76
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                     17,250,891
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                        543,711
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                        650,647
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                        178,169
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                      8,281,645
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                     10,692,915
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                      4,670,901
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                     28,109,652
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                      1,372,527
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                      7,687,633
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                     11,515,672
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                     12,327,039
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          38.64
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              12.36
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.32
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                          44.37
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         850.53
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                           5.67
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a

