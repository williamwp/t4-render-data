test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/14-Evert-cuda-sample1$ /usr/local/bin/nsys  profile  --stats=true  ./evert-cuda
WARNING: CPU IP/backtrace sampling not supported, disabling.
Try the 'nsys status --environment' command to learn more.

WARNING: CPU context switch tracing not supported, disabling.
Try the 'nsys status --environment' command to learn more.

FAIL  CUDA  evert-cuda.cu:L759 gpu_work_init  cudaErrorInvalidDevice invalid device ordinal
gpus_init 1.215612

img_w     960
img_h     540
nframes   7
nsamples  2
nbounces  4

img dir                   .
triangles nelems          57,600
triangles nbytes          2,534,400

nintersections any frame  238,878,720,000
nintersections all frames 1,672,151,040,000

0000  0.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.005596  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.676  px/s 141,042  prim/s 15,671  rays/s 1,128,334  ints/s 64,992,044,439  ppm 0.002  
0001  2.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.003374  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.576  px/s 144,958  prim/s 16,106  rays/s 1,159,664  ints/s 66,796,640,853  ppm 0.002  
0002  4.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.003757  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.593  px/s 144,287  prim/s 16,032  rays/s 1,154,296  ints/s 66,487,434,346  ppm 0.002  
0003  6.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.003722  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.616  px/s 143,380  prim/s 15,931  rays/s 1,147,042  ints/s 66,069,611,827  ppm 0.002  
0004  8.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.003832  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.629  px/s 142,832  prim/s 15,870  rays/s 1,142,655  ints/s 65,816,901,280  ppm 0.002  
0005 10.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.002805  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.636  px/s 142,568  prim/s 15,841  rays/s 1,140,544  ints/s 65,695,309,521  ppm 0.002  
0006 12.000  FAIL  CUDA  evert-cuda.cu:L809 gpu_mesh_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
mesh_shdr 0.001901  FAIL  CUDA  evert-cuda.cu:L822 gpu_pixel_shader  cudaErrorInvalidDevice invalid device ordinal
FAIL  CUDA  evert-cuda.cu:L802 gpu_sync  cudaErrorInvalidDevice invalid device ordinal
px_shdr 3.638  px/s 142,480  prim/s 15,831  rays/s 1,139,844  ints/s 65,655,013,825  ppm 0.002  

FAIL  CUDA  evert-cuda.cu:L796 gpu_work_free  cudaErrorInvalidValue invalid argument
gpus_free 0.132712
took 26.7673 seconds.
Generating '/tmp/nsys-report-d17d.qdstrm'
[1/8] [========================100%] report4.nsys-rep
[2/8] [========================100%] report4.sqlite
[3/8] Executing 'nvtx_sum' stats report
SKIPPED: /home/test/tools/backup6/GPU-rendering-programs/14-Evert-cuda-sample1/report4.sqlite does not contain NV Tools Extension (NVTX) data.
[4/8] Executing 'osrt_sum' stats report

 Time (%)  Total Time (ns)  Num Calls    Avg (ns)      Med (ns)     Min (ns)   Max (ns)    StdDev (ns)            Name         
 --------  ---------------  ---------  ------------  -------------  --------  -----------  ------------  ----------------------
     99.5   26,610,929,450        329  80,884,284.0  100,134,440.0       143  100,154,670  38,901,328.6  poll                  
      0.4      106,779,610      1,955      54,618.7       14,512.0       200   11,829,967     374,614.6  ioctl                 
      0.0        5,744,596        229      25,085.6       17,392.0     1,543      548,204      63,259.4  mmap64                
      0.0        2,548,046         54      47,186.0        4,275.5       544      228,174      70,370.6  mmap                  
      0.0        1,419,680         40      35,492.0       33,793.5     3,728      188,949      39,926.2  sem_timedwait         
      0.0        1,184,646         13      91,126.6      155,070.0     1,643      174,151      84,725.8  open                  
      0.0          857,376        161       5,325.3        4,584.0     1,886       33,159       2,891.1  open64                
      0.0          434,452         67       6,484.4        3,451.0       318      122,890      20,556.7  munmap                
      0.0          421,989         34      12,411.4        2,226.0       467      323,624      55,047.5  fopen                 
      0.0          393,957          7      56,279.6          950.0       134      278,643     106,508.3  fwrite                
      0.0          176,827          2      88,413.5       88,413.5    86,582       90,245       2,590.1  pthread_join          
      0.0          139,170         11      12,651.8        2,767.0       109       87,595      26,455.3  fread                 
      0.0           83,404          5      16,680.8       17,449.0    11,727       21,678       3,783.6  pthread_create        
      0.0           82,736         45       1,838.6        1,843.0       342        4,645         936.7  write                 
      0.0           42,033        177         237.5          190.0        99        2,164         231.9  fcntl                 
      0.0           27,692         48         576.9          600.5       171        1,683         311.4  read                  
      0.0           27,580         62         444.8          248.0        20        1,988         562.9  fflush                
      0.0           27,428         27       1,015.9          763.0       371        3,888         763.4  fclose                
      0.0           25,835         27         956.9           31.0        24       24,986       4,802.3  fgets                 
      0.0           24,581          7       3,511.6        1,176.0     1,133       17,514       6,174.7  ftruncate             
      0.0            6,345          2       3,172.5        3,172.5     3,103        3,242          98.3  pipe2                 
      0.0            6,044          2       3,022.0        3,022.0     1,497        4,547       2,156.7  socket                
      0.0            5,125          1       5,125.0        5,125.0     5,125        5,125           0.0  connect               
      0.0            1,645         64          25.7           15.0        14           88          23.1  pthread_mutex_trylock 
      0.0              778          1         778.0          778.0       778          778           0.0  bind                  
      0.0              440          1         440.0          440.0       440          440           0.0  listen                
      0.0              336          2         168.0          168.0       156          180          17.0  pthread_cond_broadcast

[5/8] Executing 'cuda_api_sum' stats report

 Time (%)  Total Time (ns)  Num Calls    Avg (ns)       Med (ns)     Min (ns)     Max (ns)       StdDev (ns)            Name         
 --------  ---------------  ---------  -------------  ------------  ----------  -------------  ---------------  ---------------------
     95.4   25,388,437,422         28  906,729,907.9     920,021.5       1,132  3,675,471,937  1,597,333,784.3  cudaStreamSynchronize
      4.1    1,092,282,697          6  182,047,116.2      32,900.0       2,213  1,092,104,135    445,835,068.6  cudaMalloc           
      0.3       70,010,685          6   11,668,447.5      10,948.5         369     69,848,628     28,502,402.0  cudaFree             
      0.2       61,666,158          2   30,833,079.0  30,833,079.0  29,750,057     31,916,101      1,531,624.4  cudaDeviceReset      
      0.0          459,122          1      459,122.0     459,122.0     459,122        459,122              0.0  cudaHostAlloc        
      0.0          357,810         42        8,519.3       3,538.0       2,174         52,258         11,158.4  cudaLaunchKernel     
      0.0           73,225         14        5,230.4       4,754.0       2,845          9,982          2,298.3  cudaMemcpyAsync      
      0.0            3,656          2        1,828.0       1,828.0       1,435          2,221            555.8  cuCtxSynchronize     
      0.0            1,556          1        1,556.0       1,556.0       1,556          1,556              0.0  cudaFreeHost         

[6/8] Executing 'cuda_gpu_kern_sum' stats report

 Time (%)  Total Time (ns)  Instances     Avg (ns)         Med (ns)        Min (ns)       Max (ns)      StdDev (ns)                                                   Name                                                
 --------  ---------------  ---------  ---------------  ---------------  -------------  -------------  -------------  ----------------------------------------------------------------------------------------------------
     99.9   25,362,709,288         14  1,811,622,092.0  1,801,356,533.0  1,362,787,670  2,312,508,633  439,408,194.0  ker_pixel_shader(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int…
      0.1       24,629,773         14      1,759,269.5      1,836,803.5        919,753      2,776,446      541,457.3  ker_mesh_shader(float, quat, unsigned int, unsigned int, triangle_t *)                              
      0.0           38,207         14          2,729.1          2,576.0          2,240          4,671          769.3  ker_lights_init(light_t *)                                                                          

[7/8] Executing 'cuda_gpu_mem_time_sum' stats report

 Time (%)  Total Time (ns)  Count  Avg (ns)  Med (ns)  Min (ns)  Max (ns)  StdDev (ns)      Operation     
 --------  ---------------  -----  --------  --------  --------  --------  -----------  ------------------
    100.0        1,116,325     14  79,737.5  79,741.5    60,862    98,622     19,582.8  [CUDA memcpy DtoH]

[8/8] Executing 'cuda_gpu_mem_size_sum' stats report

 Total (MB)  Count  Avg (MB)  Med (MB)  Min (MB)  Max (MB)  StdDev (MB)      Operation     
 ----------  -----  --------  --------  --------  --------  -----------  ------------------
     14.515     14     1.037     1.037     0.791     1.283        0.255  [CUDA memcpy DtoH]

Generated:
    /home/test/tools/backup6/GPU-rendering-programs/14-Evert-cuda-sample1/report4.nsys-rep
    /home/test/tools/backup6/GPU-rendering-programs/14-Evert-cuda-sample1/report4.sqlite

