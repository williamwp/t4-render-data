test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1$ /usr/local/cuda-11.4/bin/nsys  profile --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
^CSignal 2 (SIGINT) was forwarded to the target application process group 9682

The target application terminated with signal 2 (SIGINT)
Processing events...
Saving temporary "/tmp/nsys-report-dd92-3416-5fae-051f.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-dd92-3416-5fae-051f.qdrep"
Exporting 12594 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-dd92-3416-5fae-051f.sqlite


Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    91.4  117,356,353,974      5,924  19,810,323.1   12,501  100,164,234  27,968,993.4  poll          
     8.5   10,974,608,337      5,281   2,078,130.7    9,887   21,178,404     277,470.6  sem_timedwait 
     0.0       55,397,491        706      78,466.7    1,000   13,626,512     589,927.6  ioctl         
     0.0        1,280,042         68      18,824.1    1,511      437,236      52,040.3  mmap64        
     0.0          568,850         37      15,374.3    1,057      498,766      81,680.2  fopen         
     0.0          440,758         86       5,125.1    1,825       12,102       1,459.7  open64        
     0.0          230,087         23      10,003.8    1,390       34,526      10,293.7  fread         
     0.0           80,838         18       4,491.0    1,010       20,369       4,788.0  mmap          
     0.0           52,939          4      13,234.8   11,490       15,929       1,968.5  pthread_create
     0.0           30,784          9       3,420.4    1,209       12,221       3,653.6  munmap        
     0.0           24,555          1      24,555.0   24,555       24,555           0.0  fgets         
     0.0           20,541          8       2,567.6    1,122        5,031       1,236.9  write         
     0.0           13,359          6       2,226.5    1,338        3,983         926.5  open          
     0.0            6,328          2       3,164.0    2,240        4,088       1,306.7  fgetc         
     0.0            3,753          2       1,876.5    1,594        2,159         399.5  socket        
     0.0            3,174          1       3,174.0    3,174        3,174           0.0  connect       
     0.0            2,656          2       1,328.0    1,004        1,652         458.2  fclose        
     0.0            2,601          1       2,601.0    2,601        2,601           0.0  pipe2         
     0.0            1,794          1       1,794.0    1,794        1,794           0.0  fcntl         
     0.0            1,186          1       1,186.0    1,186        1,186           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1/report7.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1/report7.sqlite
