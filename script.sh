#!/bin/bash

# Define the list of rendering programs and their initial weights
declare -a programs=("A" "B" "C" "D")
declare -A program_weights=(
    [A]=1
    [B]=1
    [C]=1
    [D]=1
)

# Allocate resources to the rendering program
allocate_resources() {
    local program=$1
    local weight=$2
    local total_gpu_cores=100  # Assume there are 100 GPU cores available for allocation, NVIDIA Tesla P100 has 3584 GPU cores

    # Define the GPU utilization for each rendering program
    declare -A gpu_utilization=(
        [A]=10
        [B]=20
        [C]=30
        [D]=35
    )

    # Calculate the number of GPU cores the rendering program can get
    local gpu_util=${gpu_utilization[$program]}
    local allocated_gpu_cores=$((weight * total_gpu_cores * gpu_util / 100))

    # Perform the resource allocation operations, such as setting GPU core count, memory quota, etc.
    # Here, we are just printing the output. You need to modify it according to your actual resource allocation operations.
    echo "Allocating $allocated_gpu_cores GPU cores to program $program"
}

run_program() {
    local program=$1
    # Execute the corresponding executable file based on the rendering program's name
    case $program in
        "A")
            ./a1
            ;;
        "B")
            ./a2
            ;;
        "C")
            ./a3
            ;;
        "D")
            ./a4
            ;;
        *)
            echo "Unknown program: $program"
            ;;
    esac
}

# Main loop to simulate the continuous execution of programs
# while true
#do
    # Iterate over the rendering program list, allocate resources based on weights, and run the program
    for program in "${programs[@]}"
    do
        allocate_resources "$program" "${program_weights[$program]}"
        run_program "$program"
	echo -e "\n"
    done
    sleep 1  # Adjust the sleep time as needed
    echo -e "\n"
#done
