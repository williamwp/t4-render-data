Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-184e-03ab-85fa-c37e.qdrep"
Exporting 21646 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-184e-03ab-85fa-c37e.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls     Average       Minimum      Maximum    StdDev          Name         
 -------  ---------------  ---------  -------------  -----------  -----------  ------  ---------------------
    88.4      109,491,396          1  109,491,396.0  109,491,396  109,491,396     0.0  cudaMallocManaged    
     6.4        7,936,987          1    7,936,987.0    7,936,987    7,936,987     0.0  cudaDeviceSynchronize
     5.1        6,296,474          1    6,296,474.0    6,296,474    6,296,474     0.0  cudaFree             
     0.1           66,223          1       66,223.0       66,223       66,223     0.0  cudaLaunchKernel     



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances    Average     Minimum    Maximum   StdDev           Name          
 -------  ---------------  ---------  -----------  ---------  ---------  ------  -----------------------
   100.0        7,940,251          1  7,940,251.0  7,940,251  7,940,251     0.0  render(vec3*, int, int)



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum   StdDev               Operation            
 -------  ---------------  ----------  --------  -------  -------  --------  ---------------------------------
   100.0        9,587,043         924  10,375.6    1,662  103,484  18,814.4  [CUDA Unified Memory memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

    Total     Operations  Average  Minimum   Maximum   StdDev               Operation            
 -----------  ----------  -------  -------  ---------  -------  ---------------------------------
 103,680.000         924  112.208    4.000  1,008.000  239.158  [CUDA Unified Memory memcpy DtoH]



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    75.1   15,007,301,428        764  19,643,064.7   12,776  100,412,518  27,824,938.2  poll          
    17.5    3,486,187,756     17,956     194,151.7    1,000  110,932,210   3,171,639.0  fwrite        
     7.2    1,428,734,339        682   2,094,918.4   10,034   21,280,638     843,110.8  sem_timedwait 
     0.2       40,494,927        647      62,588.8    1,009    7,590,712     399,314.6  ioctl         
     0.0        6,283,383         16     392,711.4    1,018    6,218,043   1,553,429.6  mmap          
     0.0        1,335,131         68      19,634.3    1,599      466,212      55,487.4  mmap64        
     0.0          673,703         26      25,911.7    1,096      624,492     122,090.1  fopen         
     0.0          458,962         86       5,336.8    1,869       11,683       1,517.4  open64        
     0.0           57,391          4      14,347.8   12,999       15,453       1,283.5  pthread_create
     0.0           25,172          1      25,172.0   25,172       25,172           0.0  fgets         
     0.0           22,209         10       2,220.9    1,042        4,791       1,137.9  write         
     0.0           14,574          6       2,429.0    1,769        4,544       1,057.2  munmap        
     0.0           13,810          6       2,301.7    1,393        4,415       1,088.3  open          
     0.0           13,460          4       3,365.0    1,382        8,279       3,303.5  fread         
     0.0            6,693          2       3,346.5    2,154        4,539       1,686.4  fgetc         
     0.0            3,349          2       1,674.5    1,372        1,977         427.8  socket        
     0.0            3,205          1       3,205.0    3,205        3,205           0.0  pipe2         
     0.0            3,144          1       3,144.0    3,144        3,144           0.0  connect       
     0.0            2,628          2       1,314.0    1,024        1,604         410.1  fclose        
     0.0            2,210          2       1,105.0    1,051        1,159          76.4  read          
     0.0            1,829          1       1,829.0    1,829        1,829           0.0  fcntl         
     0.0            1,591          1       1,591.0    1,591        1,591           0.0  bind          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/82-Rendering-background-vec3-sample3/report4.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/82-Rendering-background-vec3-sample3/report4.sqlite"

