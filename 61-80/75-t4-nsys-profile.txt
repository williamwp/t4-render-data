/nsys  profile --stats=true  ./run -s scene9.json 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Using following defaults:
+ Width: 512
+ Height: 512
+ MaxRayDepth: 3
+ Resources: resources.json
+ Image: image.png
Oct taken seconds: 2.53774
Rendering seconds: 0.720983
Processing events...
Saving temporary "/tmp/nsys-report-a61d-0b81-2d16-2d5b.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-a61d-0b81-2d16-2d5b.qdrep"
Exporting 34201 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-a61d-0b81-2d16-2d5b.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls     Average       Minimum      Maximum      StdDev             Name         
 -------  ---------------  ---------  -------------  -----------  -----------  -----------  ---------------------
    80.6      681,534,871          1  681,534,871.0  681,534,871  681,534,871          0.0  cudaDeviceSynchronize
    12.7      107,471,506      5,334       20,148.4           79   97,315,589  1,332,440.5  cudaMalloc           
     4.5       38,294,476      5,208        7,353.0           76      292,450      5,022.0  cudaMemcpy           
     1.4       11,440,731     15,212          752.1          169      224,520      2,019.1  cudaFree             
     0.8        6,788,712      1,012        6,708.2        2,026    4,474,651    140,589.9  cudaLaunchKernel     



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances     Average       Minimum      Maximum    StdDev                               Name                             
 -------  ---------------  ---------  -------------  -----------  -----------  ------  --------------------------------------------------------------
    99.2      681,524,349          1  681,524,349.0  681,524,349  681,524,349     0.0  film(unsigned char*, DCamera*, DLights*, DSpheres*, DOctTree*)
     0.7        5,148,399      1,008        5,107.5        5,023       14,176   379.4  initChildNode(DOctTree**, int, DOctTree*)                     
     0.0           15,039          3        5,013.0        4,704        5,279   289.9  iniMats(DSphere*, DMaterial**)                                



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum  StdDev      Operation     
 -------  ---------------  ----------  --------  -------  -------  ------  ------------------
    99.1        6,420,200       4,953   1,296.2    1,184    1,728   101.8  [CUDA memcpy HtoD]
     0.9           61,118           1  61,118.0   61,118   61,118     0.0  [CUDA memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

  Total   Operations  Average  Minimum  Maximum  StdDev      Operation     
 -------  ----------  -------  -------  -------  ------  ------------------
 768.000           1  768.000  768.000  768.000   0.000  [CUDA memcpy DtoH]
 492.363       4,953    0.099    0.008    0.391   0.049  [CUDA memcpy HtoD]



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    98.4    3,205,051,730         44  72,842,084.8   13,152  100,148,739  43,448,111.2  poll          
     1.5       49,045,021        703      69,765.3    1,003    7,218,003     393,720.4  ioctl         
     0.0        1,295,883         68      19,057.1    1,497      460,532      54,825.3  mmap64        
     0.0          594,186         26      22,853.3    1,182      525,001     102,490.5  fopen         
     0.0          574,649         11      52,240.8    9,150      169,986      41,401.5  sem_timedwait 
     0.0          442,768         86       5,148.5    1,811       11,075       1,405.3  open64        
     0.0          132,488          8      16,561.0    1,268       75,280      25,253.0  fread         
     0.0           73,047         19       3,844.6    1,009       21,367       4,474.5  mmap          
     0.0           51,048          4      12,762.0   11,284       14,497       1,443.6  pthread_create
     0.0           43,073         25       1,722.9    1,002        6,267       1,326.6  read          
     0.0           25,304          9       2,811.6    1,082        5,168       1,341.4  write         
     0.0           24,533          1      24,533.0   24,533       24,533           0.0  fgets         
     0.0           21,226         10       2,122.6    1,239        3,231         672.1  munmap        
     0.0           19,078          4       4,769.5    1,061       15,039       6,856.7  fclose        
     0.0           16,295          2       8,147.5    1,947       14,348       8,768.8  fcntl         
     0.0           13,564          6       2,260.7    1,410        4,271       1,021.1  open          
     0.0           11,611          3       3,870.3    1,293        7,529       3,255.6  fwrite        
     0.0            9,071          4       2,267.8    1,975        2,434         201.3  fopen64       
     0.0            5,925          2       2,962.5    2,559        3,366         570.6  putc          
     0.0            5,460          2       2,730.0    2,425        3,035         431.3  fgetc         
     0.0            3,666          2       1,833.0    1,444        2,222         550.1  socket        
     0.0            3,537          1       3,537.0    3,537        3,537           0.0  connect       
     0.0            2,670          1       2,670.0    2,670        2,670           0.0  pipe2         

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/75-3D-stanford-bunny-sample3/report8.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/75-3D-stanford-bunny-sample3/report8.sqlite
