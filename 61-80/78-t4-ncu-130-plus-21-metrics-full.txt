==PROF== Connected to process 31253 (/home/test/tools/backup6/GPU-rendering-programs/78-Ray-tracer-texture-sample3/a.out)
created the scene successfully 
this one worked 
==PROF== Profiling "render" - 1: 0%....50%....100% - 13 passes
kernel worked
shits successful 
took 8.77542 seconds.
==PROF== Disconnected from process 31253
[31253] a.out@127.0.0.1
  render(RGBType *, int, int, double, int, int, double, double, double, int, sphere *, int, plane *, int, light *, int, camera), 2023-Mar-01 06:43:07, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Mbyte                          12.41
    dram__bytes_read.sum.per_second                                           Mbyte/second                          21.67
    dram__bytes_write.sum                                                            Mbyte                         344.71
    dram__bytes_write.sum.per_second                                          Mbyte/second                         602.02
    dram__sectors_read.sum                                                          sector                        387,833
    dram__sectors_write.sum                                                         sector                     10,772,267
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.20
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         585.00
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           0.93
    l1tex__lsu_writeback_active.sum                                                  cycle                    124,688,978
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           1.11
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                         150.54
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                         262.91
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                          18.70
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                     19,794,867
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                        196,020
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                     84,499,008
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                     84,166,386
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          97.37
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                     19,794,867
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                    334,655,378
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                    333,376,106
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/usecond                         582.22
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                         624.22
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                          50.09
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          99.94
    lts__t_sector_op_read_hit_rate.pct                                                   %                         101.90
    lts__t_sector_op_write_hit_rate.pct                                                  %                          99.94
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           2.23
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                             42
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                   sector/second                          73.35
    lts__t_sectors_op_read.sum                                                      sector                     14,209,563
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                          24.82
    lts__t_sectors_op_write.sum                                                     sector                    334,972,585
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                         585.01
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                      7,351,453
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                          12.84
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.00
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.00
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          99.92
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           0.47
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          24.80
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                      23,718.25
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          99.59
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.03
    smsp__inst_executed.sum                                                           inst                  1,561,230,281
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                     19,230,129
    smsp__inst_executed_op_global_st.sum                                              inst                        196,020
    smsp__inst_executed_op_local_ld.sum                                               inst                     83,981,718
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                        654,368
    smsp__inst_executed_pipe_cbu.sum                                                  inst                     90,187,897
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                          21.66
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           0.71
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.03
    smsp__inst_issued.sum                                                             inst                  1,561,283,917
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                           2.93
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                  2,977,502,357
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                  7,956,011,434
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                 10,982,833,666
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                  2,365,657,526
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                    200,520,718
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                  1,682,675,294
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                 22,365,493,430
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                 10,263,516,926
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                  5,881,206,448
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                  2,539,826,997
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          97.97
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              31.35
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.00
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                           4.75
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         855.00
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          24.78
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a

