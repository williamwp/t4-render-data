test@tesla-t4:~/tools/backup6/GPU-rendering-programs/70-Cuda-ray-shadows-sample3$ /usr/local/cuda-11.4/bin/nsys   profile  --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Time per frame : 92.7173 seconds
Time per frame : 92.7476 seconds
Time per frame : 92.6891 seconds
Time per frame : 92.0335 seconds
Time per frame : 91.796 seconds
Time per frame : 91.0287 seconds
Time per frame : 91.7398 seconds
Time per frame : 91.3619 seconds
Time per frame : 91.093 seconds
Time per frame : 91.2127 seconds
Time per frame : 93.6456 seconds
Time per frame : 94.9371 seconds
Time per frame : 94.3697 seconds
Time per frame : 93.6611 seconds
Time per frame : 94.0323 seconds
Time per frame : 95.2788 seconds
Time per frame : 97.7101 seconds
Time per frame : 96.7181 seconds
Time per frame : 98.6721 seconds
Time per frame : 95.3208 seconds
Time per frame : 95.6986 seconds
Time per frame : 93.6625 seconds
Time per frame : 94.06 seconds
Time per frame : 95.0758 seconds
Time per frame : 91.8732 seconds
Time per frame : 88.0231 seconds
Time per frame : 84.923 seconds
Time per frame : 90.4125 seconds
Time per frame : 97.6402 seconds
Time per frame : 98.856 seconds
Time per frame : 99.4012 seconds
Time per frame : 98.778 seconds
Time per frame : 98.7013 seconds
Time per frame : 98.1158 seconds
Time per frame : 100.121 seconds
Time per frame : 100.651 seconds
Time per frame : 101.99 seconds
Time per frame : 99.9196 seconds
Time per frame : 99.5821 seconds
Time per frame : 97.4883 seconds
Time per frame : 97.5311 seconds
Time per frame : 97.5649 seconds
Time per frame : 97.4237 seconds
Time per frame : 98.8615 seconds
Time per frame : 97.99 seconds
Time per frame : 99.3979 seconds
Time per frame : 98.4083 seconds
Time per frame : 100.037 seconds
Time per frame : 99.6354 seconds
Time per frame : 98.3307 seconds
Time per frame : 99.5408 seconds
Time per frame : 99.7805 seconds
Time per frame : 98.712 seconds
Time per frame : 101.432 seconds
Time per frame : 102.012 seconds
Time per frame : 101.119 seconds
Time per frame : 101.898 seconds
Time per frame : 100.673 seconds
Time per frame : 99.1693 seconds
Time per frame : 100.726 seconds
Time per frame : 100.272 seconds
Time per frame : 98.3091 seconds
Time per frame : 94.7882 seconds
Time per frame : 93.7918 seconds
Time per frame : 95.7446 seconds
Time per frame : 94.604 seconds
Time per frame : 94.1888 seconds
Time per frame : 99.815 seconds
Time per frame : 100.384 seconds
Time per frame : 97.0868 seconds
Time per frame : 97.9239 seconds
Time per frame : 96.6771 seconds
Time per frame : 93.2356 seconds
Time per frame : 92.2802 seconds
Time per frame : 90.436 seconds
Time per frame : 86.9354 seconds
Time per frame : 88.4872 seconds
Time per frame : 90.4876 seconds
Time per frame : 90.6648 seconds
Time per frame : 90.6123 seconds
Time per frame : 90.5589 seconds
Time per frame : 94.8666 seconds
Time per frame : 92.8934 seconds
Time per frame : 90.2154 seconds
Time per frame : 91.6813 seconds
Time per frame : 91.7041 seconds
Time per frame : 92.6926 seconds
Time per frame : 93.4945 seconds
Time per frame : 93.6475 seconds
Time per frame : 92.4685 seconds
Time per frame : 93.2349 seconds
Time per frame : 92.3343 seconds
Time per frame : 90.8248 seconds
Time per frame : 91.5394 seconds
Time per frame : 89.8689 seconds
Time per frame : 90.8238 seconds
Time per frame : 89.7002 seconds
Time per frame : 91.827 seconds
Time per frame : 93.2908 seconds
Time per frame : 91.6882 seconds
took 9517.82 seconds.
Processing events...
Saving temporary "/tmp/nsys-report-4b9d-4cd1-e900-3df9.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-4b9d-4cd1-e900-3df9.qdrep"
Exporting 1682779 events: [===============================================100%]

Exported successfully to
/tmp/nsys-report-4b9d-4cd1-e900-3df9.sqlite


CUDA API Statistics:

 Time(%)   Total Time (ns)   Num Calls      Average         Minimum        Maximum           StdDev               Name         
 -------  -----------------  ---------  ----------------  -----------  ---------------  ----------------  ---------------------
   100.0  9,493,347,226,772        202  46,996,768,449.4       82,408  101,771,289,898  47,639,106,301.3  cudaDeviceSynchronize
     0.0        129,783,895          1     129,783,895.0  129,783,895      129,783,895               0.0  cudaMallocManaged    
     0.0         45,199,767          1      45,199,767.0   45,199,767       45,199,767               0.0  cudaDeviceReset      
     0.0         41,008,156          5       8,201,631.2        2,856       38,761,112      17,102,454.6  cudaFree             
     0.0          5,373,417        303          17,734.0        3,291        1,423,726          81,434.4  cudaLaunchKernel     
     0.0            650,977          4         162,744.3        2,379          530,718         250,807.1  cudaMalloc           
     0.0              2,732          1           2,732.0        2,732            2,732               0.0  cuCtxSynchronize     



CUDA Kernel Statistics:

 Time(%)   Total Time (ns)   Instances      Average          Minimum          Maximum          StdDev                                          Name                                   
 -------  -----------------  ---------  ----------------  --------------  ---------------  ---------------  --------------------------------------------------------------------------
   100.0  9,491,054,366,985        100  94,910,543,669.9  84,724,880,921  101,771,260,008  3,914,028,550.7  render(vec3*, int, int, int, camera**, hitable**, curandStateXORWOW*)     
     0.0      2,240,052,405          1   2,240,052,405.0   2,240,052,405    2,240,052,405              0.0  render_init(int, int, curandStateXORWOW*)                                 
     0.0         42,297,747          1      42,297,747.0      42,297,747       42,297,747              0.0  create_world(hitable**, hitable**, curandStateXORWOW*)                    
     0.0         38,749,679          1      38,749,679.0      38,749,679       38,749,679              0.0  free_world(hitable**, hitable**)                                          
     0.0          8,959,133        100          89,591.3          78,942           90,398          1,109.5  init_cam(camera**, unsigned int, unsigned int, unsigned int, unsigned int)
     0.0          7,369,153        100          73,691.5          71,870           83,294          1,540.6  delete_cam(camera**)                                                      



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum   StdDev               Operation            
 -------  ---------------  ----------  --------  -------  -------  --------  ---------------------------------
    54.5       94,106,321       8,810  10,681.8    2,270  105,277  16,473.0  [CUDA Unified Memory memcpy HtoD]
    45.5       78,446,084       7,300  10,746.0    1,727  101,436  19,430.6  [CUDA Unified Memory memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

    Total     Operations  Average  Minimum   Maximum   StdDev               Operation            
 -----------  ----------  -------  -------  ---------  -------  ---------------------------------
 844,000.000       7,300  115.616    4.000  1,020.000  248.534  [CUDA Unified Memory memcpy DtoH]
 835,560.000       8,810   94.842    4.000  1,020.000  198.852  [CUDA Unified Memory memcpy HtoD]



Operating System Runtime API Statistics:

 Time(%)   Total Time (ns)    Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ------------------  ---------  ------------  -------  -----------  ------------  --------------
    91.4  17,359,627,214,609    874,447  19,852,120.5    3,135  100,341,912  27,998,455.0  poll          
     8.6   1,623,920,265,306    779,639   2,082,913.1    3,608   21,200,467      33,177.7  sem_timedwait 
     0.0       1,933,890,285     10,480     184,531.5    1,002   32,098,452   1,763,028.3  ioctl         
     0.0           3,430,640         23     149,158.3    1,055      643,104     216,006.3  fread         
     0.0           3,341,147        115      29,053.5    1,782      717,480      79,228.6  mmap64        
     0.0           3,176,523         27     117,649.0    1,044    1,885,586     361,726.6  mmap          
     0.0           1,502,828        141      10,658.4    1,108      552,992      49,912.0  fopen         
     0.0             491,682         88       5,587.3    2,020       16,019       1,878.8  open64        
     0.0             347,610        100       3,476.1    2,805       17,480       1,434.2  putc          
     0.0             340,388         38       8,957.6    1,024      124,053      20,959.1  munmap        
     0.0              82,744          4      20,686.0   12,372       39,220      12,534.6  pthread_create
     0.0              68,094          1      68,094.0   68,094       68,094           0.0  pthread_join  
     0.0              37,475         16       2,342.2    1,100        4,116         736.4  write         
     0.0              24,387          1      24,387.0   24,387       24,387           0.0  fgets         
     0.0              18,822         11       1,711.1    1,028        5,451       1,296.9  fclose        
     0.0              14,264          6       2,377.3    1,289        4,476       1,095.0  open          
     0.0               8,200          2       4,100.0    2,119        6,081       2,801.6  fwrite        
     0.0               6,146          2       3,073.0    2,314        3,832       1,073.4  fgetc         
     0.0               4,582          2       2,291.0    1,839        2,743         639.2  socket        
     0.0               3,493          1       3,493.0    3,493        3,493           0.0  connect       
     0.0               2,721          1       2,721.0    2,721        2,721           0.0  pipe2         
     0.0               1,853          1       1,853.0    1,853        1,853           0.0  fcntl         
     0.0               1,276          1       1,276.0    1,276        1,276           0.0  read 
