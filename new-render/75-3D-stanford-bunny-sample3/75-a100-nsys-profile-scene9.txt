wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/GPU-rendering-programs/75-3D-stanford-bunny-sample3$ /home/chenjiuyi/usr_chenjiuyi/local/cuda-11.1/bin/nsys  profile --stats=true ./run -s scene9.json
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Using following defaults:
+ Width: 512
+ Height: 512
+ MaxRayDepth: 3
+ Resources: resources.json
+ Image: image.png
Oct taken seconds: 4.66513
Rendering seconds: 0.800587
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-c7d6-b891-a2ea-d377.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-c7d6-b891-a2ea-d377.qdrep"
Exporting 34649 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-c7d6-b891-a2ea-d377.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   79.6      3231055353        5334        605747.2             121      3209998312  cudaMalloc                                                                      
   18.3       741669386           1     741669386.0       741669386       741669386  cudaDeviceSynchronize                                                           
    1.4        56847016        5208         10915.3             113          441510  cudaMemcpy                                                                      
    0.5        18370658       15212          1207.6             245         1298237  cudaFree                                                                        
    0.2        10140554        1012         10020.3            2574         7035763  cudaLaunchKernel                                                                





Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.4        11374987        4953          2296.6            2175            2945  [CUDA memcpy HtoD]                                                              
    0.6           69535           1         69535.0           69535           69535  [CUDA memcpy DtoH]                                                              


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
            768.000               1              768.000            768.000              768.000  [CUDA memcpy DtoH]                                                              
            492.363            4953                0.099              0.008                0.391  [CUDA memcpy HtoD]                                                              




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   75.8      7414203347          86      86211666.8           33733       100234356  poll                                                                            
   24.1      2351868636        1042       2257071.6            1072      1263071171  ioctl                                                                           
    0.1         5383834          43        125205.4            1190         5191138  fopen                                                                           
    0.0         2552068          84         30381.8            1002          817937  mmap                                                                            
    0.0         1517793          89         17053.9            5145          117041  open64                                                                          
    0.0         1455990          11        132362.7           12870          691349  sem_timedwait                                                                   
    0.0          159679           3         53226.3           50938           56715  fgets                                                                           
    0.0          154601           4         38650.3           33567           52294  pthread_create                                                                  
    0.0          134365          52          2583.9            1090            6005  read                                                                            
    0.0           67624          25          2705.0            1040           31007  fclose                                                                          
    0.0           66206          10          6620.6            1621           25895  fread                                                                           
    0.0           54115          12          4509.6            2137            6860  write                                                                           
    0.0           46083          10          4608.3            1580           15441  munmap                                                                          
    0.0           29742           5          5948.4            3460            9939  open                                                                            
    0.0           26199           5          5239.8            1248           16719  fwrite                                                                          
    0.0           21304           4          5326.0            3532            6275  fopen64                                                                         
    0.0           13559           2          6779.5            5963            7596  putc                                                                            
    0.0           11275           5          2255.0            1133            3520  fcntl                                                                           
    0.0            7985           2          3992.5            3378            4607  socket                                                                          
    0.0            6606           1          6606.0            6606            6606  connect                                                                         
    0.0            5879           1          5879.0            5879            5879  pipe2                                                                           




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/75-3D-stanford-bunny-sample3/report9.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/75-3D-stanford-bunny-sample3/report9.sqlite"
