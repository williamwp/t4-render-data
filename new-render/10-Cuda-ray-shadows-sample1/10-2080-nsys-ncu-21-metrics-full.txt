test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1$ sudo /usr/local/cuda-11.4/bin/ncu --metrics  gpc__cycles_elapsed.avg.per_second,sys__cycles_elapsed.avg.per_second,gr__cycles_active.sum.pct_of_peak_sustained_elapsed,gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapsed,gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elapsed,fe__draw_count.avg.pct_of_peak_sustained_elapsed,gr__dispatch_count.avg.pct_of_peak_sustained_elapsed,tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapsed,tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed,tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed,tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elapsed,tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed,sm__cycles_active.avg.pct_of_peak_sustained_elapsed,sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elapsed,sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed,sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elapsed,dram__throughput.avg.pct_of_peak_sustained_elapsed,pcie__read_bytes.avg.pct_of_peak_sustained_elapsed,pcie__write_bytes.avg.pct_of_peak_sustained_elapsed,pcie__rx_requests_aperture_bar1_op_read.sum,pcie__rx_requests_aperture_bar1_op_write.sum  ./a.out 
[sudo] password for test: 
==PROF== Connected to process 26288 (/home/test/tools/backup6/GPU-rendering-programs/10-Cuda-ray-shadows-sample1/a.out)
==PROF== Profiling "render_init" - 1: 0%....50%....100% - 4 passes
==PROF== Profiling "create_world" - 2: 0%....50%....100% - 4 passes
==PROF== Profiling "init_cam" - 3: 0%....50%....100% - 4 passes
==PROF== Profiling "render" - 4: 0%....50%....100%
==ERROR== UnknownError
==ERROR== Failed to profile kernel "render" in process 26288
CUDA error at main.cu 89: the launch timed out and was terminated
==PROF== Disconnected from process 26288
==ERROR== The application returned an error code (11).
==ERROR== An error occurred while trying to profile.
[26288] a.out@127.0.0.1
  render_init(int, int, curandStateXORWOW *), 2023-Feb-25 13:40:48, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                          79.52
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.36
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.00
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.00
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          97.73
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.35
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          93.02
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  create_world(hitable **, hitable **, curandStateXORWOW *), 2023-Feb-25 13:40:48, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.00
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.36
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.00
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.00
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                           1.47
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.35
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                           0.05
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  init_cam(camera **, unsigned int, unsigned int, unsigned int, unsigned int), 2023-Feb-25 13:40:48, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.05
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.36
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                              0
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.50
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                           1.42
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.35
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                           0.04
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

