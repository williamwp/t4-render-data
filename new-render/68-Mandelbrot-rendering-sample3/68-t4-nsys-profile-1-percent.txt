Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-79d6-7f49-1b6e-f412.qdrep"
Exporting 121843 events: [================================================100%]

Exported successfully to
/tmp/nsys-report-79d6-7f49-1b6e-f412.sqlite


Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    91.4  250,030,395,559     12,610  19,827,945.7   12,354  100,170,003  27,985,727.7  poll          
     8.5   23,376,099,862     11,242   2,079,354.2    9,341   20,925,432     188,010.6  sem_timedwait 
     0.1      197,249,285     96,463       2,044.8    1,000      374,451       2,863.8  fwrite        
     0.0       45,802,146        711      64,419.3    1,007   12,122,424     538,837.2  ioctl         
     0.0       21,216,792         85     249,609.3    1,009    1,241,573     156,085.0  fclose        
     0.0       20,688,916        107     193,354.4    1,070      534,222     139,263.2  fopen         
     0.0        1,333,638         68      19,612.3    1,662      472,394      56,230.0  mmap64        
     0.0          476,920         86       5,545.6    2,029       20,562       2,185.0  open64        
     0.0           66,435         15       4,429.0    1,043       20,507       4,707.5  mmap          
     0.0           55,763          4      13,940.8   12,037       16,070       1,945.8  pthread_create
     0.0           23,622          1      23,622.0   23,622       23,622           0.0  fgets         
     0.0           21,154         10       2,115.4    1,012        4,174       1,021.2  write         
     0.0           14,507          6       2,417.8    1,440        3,934         897.4  open          
     0.0           11,766          6       1,961.0    1,111        2,729         558.5  munmap        
     0.0            9,422          3       3,140.7    2,105        3,909         931.2  fgetc         
     0.0            7,074          4       1,768.5    1,451        2,252         371.3  fread         
     0.0            3,554          2       1,777.0    1,447        2,107         466.7  socket        
     0.0            2,992          1       2,992.0    2,992        2,992           0.0  pipe2         
     0.0            2,970          1       2,970.0    2,970        2,970           0.0  connect       
     0.0            1,872          1       1,872.0    1,872        1,872           0.0  fcntl         
     0.0            1,477          1       1,477.0    1,477        1,477           0.0  fflush        
     0.0            1,223          1       1,223.0    1,223        1,223           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/68-Mandelbrot-rendering-sample3/report5.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/68-Mandelbrot-rendering-sample3/report5.sqlite"

