Plotting image 5483 with zoom 90384242488889134832149360168159404561693258205177525374597592218520508247941348146748810848360756500956524918088670310622122171926135320779236771610096592194330076614309329118132822504000431590770937462175088867326267257520128.000000 (99%)...
Plotting image 5484 with zoom 99422666737778049367391476494650046466060560601721011022125312005007330972391663575170122792612996573785484665415227587080928160354392395461046898807867024039326038406116432020990838980089394388180782785257141208317544330428416.000000 (100%)...
took 5173.96 seconds.
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-c6cb-adc6-4246-a755.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]

**** Analysis failed with:
Status: TargetProfilingFailed
Props {
  Items {
    Type: DeviceId
    Value: "Local (CLI)"
  }
}
Error {
  Type: RuntimeError
  SubError {
    Type: ProcessEventsError
    Props {
      Items {
        Type: ErrorText
        Value: "/build/agent/work/20a3cfcd1c25021d/QuadD/Host/Analysis/Modules/Event.h(166): Throw in function void QuadDAnalysis::EventI<EventBase>::NormalizeToStartTimestamp(QuadDAnalysis::QuadDTimestamp) [with EventBase = FlatData::IntermediateObject<QuadDAnalysis::FlatData::EventInternal, QuadDAnalysis::EmptyDeallocator>; QuadDAnalysis::QuadDTimestamp = std::chrono::duration<long int, std::ratio<1l, 1000000000l> >]\nDynamic exception type: boost::exception_detail::clone_impl<QuadDCommon::InvalidArgumentException>\nstd::exception::what: InvalidArgumentException\n[QuadDCommon::tag_error_text*] = CudaGpuMemory [80] event\'s start timestamp 2657274551280ns is greater than end timestamp 2657274550793ns\n"
      }
    }
  }
}


**** Errors occurred while processing the raw events. ****
**** Please see the Diagnostics Summary page after opening the qdrep file in GUI. ****

Saved report file to "/tmp/nsys-report-c6cb-adc6-4246-a755.qdrep"
Exporting 1709061 events: [===============================================100%]

Exported successfully to
/tmp/nsys-report-c6cb-adc6-4246-a755.sqlite


CUDA API Statistics:

 Time(%)   Total Time (ns)   Num Calls     Average       Minimum       Maximum             Name         
 -------  -----------------  ---------  -------------  -----------  -------------  ---------------------
    99.9  1,209,059,239,626      5,485  220,430,125.7   61,954,370  1,339,053,553  cudaDeviceSynchronize
     0.1        747,696,997          1  747,696,997.0  747,696,997    747,696,997  cudaMallocManaged    
     0.0        189,134,771      5,485       34,482.2       17,287        173,644  cudaLaunchKernel     
     0.0          1,974,170          1    1,974,170.0    1,974,170      1,974,170  cudaFree             



Operating System Runtime API Statistics:

 Time(%)   Total Time (ns)   Num Calls    Average     Minimum    Maximum            Name         
 -------  -----------------  ---------  ------------  -------  -----------  ---------------------
    91.4  9,398,310,997,314    472,961  19,871,217.7    1,678  100,207,899  poll                 
     8.6    881,010,372,262    421,579   2,089,787.1   18,201   21,031,747  sem_timedwait        
     0.0      4,756,148,888    790,631       6,015.6    1,000   70,036,588  fwrite               
     0.0      1,577,887,381      5,509     286,419.9    1,289   85,410,801  fclose               
     0.0        664,171,629      5,515     120,430.0    1,771    4,247,294  fopen                
     0.0        207,344,027        836     248,019.2    1,049   81,647,570  ioctl                
     0.0          6,326,674        102      62,026.2    1,591    1,645,086  mmap                 
     0.0          1,934,516         98      19,740.0    6,293       40,803  open64               
     0.0            259,658          3      86,552.7   83,645       91,404  fgets                
     0.0            182,366          4      45,591.5   42,636       51,512  pthread_create       
     0.0             42,158          8       5,269.8    2,540        9,067  munmap               
     0.0             36,494          5       7,298.8    4,504       10,895  open                 
     0.0             32,994         10       3,299.4    2,050        4,881  write                
     0.0             32,640          7       4,662.9    2,209        7,412  fread                
     0.0             32,487         12       2,707.3    1,584        3,610  read                 
     0.0             23,388          4       5,847.0    1,197       10,265  fgetc                
     0.0             20,604         13       1,584.9    1,007        4,405  fcntl                
     0.0             11,918          2       5,959.0    5,333        6,585  socket               
     0.0              9,934          1       9,934.0    9,934        9,934  pipe2                
     0.0              8,298          1       8,298.0    8,298        8,298  connect              
     0.0              4,535          1       4,535.0    4,535        4,535  pthread_mutex_trylock
     0.0              2,282          1       2,282.0    2,282        2,282  bind                 
     0.0              1,142          1       1,142.0    1,142        1,142  listen               

Report file moved to "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/68-Mandelbrot-rendering-sample3/report11.qdstrm"
Report file moved to "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/68-Mandelbrot-rendering-sample3/report11.qdrep"

