
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-7a7d-efec-87a6-45a9.qdrep"
Exporting 3027 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-7a7d-efec-87a6-45a9.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   97.0       435656977          12      36304748.1            2936       433209552  cudaMalloc                                                                      
    2.7        12117180           9       1346353.3            4160         7023203  cudaMemcpy                                                                      
    0.3         1256500          11        114227.3            2564          337327  cudaFree                                                                        
    0.0           94612           9         10512.4            5218           25456  cudaLaunchKernel                                                                




Generating CUDA Kernel Statistics...
CUDA Kernel Statistics (nanoseconds)

Time(%)      Total Time   Instances         Average         Minimum         Maximum  Name                                                                                                                                                                                                                                                                                                                                         
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------------------------------------------                                                                                                                                                                                                                         
   95.8         4304825           1       4304825.0         4304825         4304825  cuda_draw_polygons(polygon_t*, int, color_st*, float*, int*, int, int)                                                                                                                                                                                                                                                                       
    1.1           50976           1         50976.0           50976           50976  cuda_add_normals(vertex_st*, ivec3_t*, int)                                                                                                                                                                                                                                                                                                  
    0.9           41600           1         41600.0           41600           41600  cuda_blur_9X9_horizontal(vec3_t*, vec3_t*, int, int)                                                                                                                                                                                                                                                                                         
    0.9           38560           1         38560.0           38560           38560  cuda_blur_9X9_vertical(vec3_t*, vec3_t*, int, int)                                                                                                                                                                                                                                                                                           
    0.5           24544           1         24544.0           24544           24544  cuda_clear_buffers(color_st*, float*, int*, int)                                                                                                                                                                                                                                                                                             
    0.5           21056           1         21056.0           21056           21056  cuda_create_polygons(polygon_t*, vertex_st*, ivec3_t*, int, int, int, mat4_t*)                                                                                                                                                                                                                                                               
    0.1            4864           1          4864.0            4864            4864  cuda_normalize_normals(vertex_st*, int)                                                                                                                                                                                                                                                                                                      
    0.1            4640           1          4640.0            4640            4640  cuda_transform_vertices(vertex_st*, mat4_t*, int)                                                                                                                                                                                                                                                                                            
    0.1            4128           1          4128.0            4128            4128  cuda_light_vertices(vertex_st*, vec3_t*, vec3_t*, int)                                                                                                                                                                                                                                                                                       



Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   61.0         3593523           2       1796761.5         1531560         2061963  [CUDA memcpy DtoH]                                                              
   39.0         2293485           7        327640.7            2272         2235949  [CUDA memcpy HtoD]                                                              


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
          15625.000               2             7812.500           3906.250            11718.750  [CUDA memcpy DtoH]                                                              
          12190.437               7             1741.491              0.012            11718.750  [CUDA memcpy HtoD]                                                              




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   78.9       500109806          16      31256862.9           19466       100181765  poll                                                                            
   19.4       123152085         976        126180.4            1044        17453354  ioctl                                                                           
    0.8         4946419          55         89934.9            1214         4670629  fopen                                                                           
    0.3         2115533          79         26778.9            1089          631691  mmap                                                                            
    0.2         1380981          89         15516.6            5460           32518  open64                                                                          
    0.2         1174159          10        117415.9           14248          711097  sem_timedwait                                                                   
    0.1          477299         186          2566.1            1213           55347  fgets                                                                           
    0.0          196690           4         49172.5           48167           50530  pthread_create                                                                  
    0.0          193689          33          5869.4            2354           19602  fwrite                                                                          
    0.0          105664          28          3773.7            1003           62303  fclose                                                                          
    0.0           58388          17          3434.6            1373            9630  fread                                                                           
    0.0           40449          11          3677.2            2116            5795  write                                                                           
    0.0           33052           5          6610.4            3834           10092  open                                                                            
    0.0           26394          11          2399.5            1126            2985  read                                                                            
    0.0           20744           7          2963.4            1535            5734  munmap                                                                          
    0.0           10943           6          1823.8            1350            3449  fcntl                                                                           
    0.0            9443           2          4721.5            4172            5271  socket                                                                          
    0.0            6569           1          6569.0            6569            6569  connect                                                                         
    0.0            5448           1          5448.0            5448            5448  pipe2                                                                           
    0.0            1477           1          1477.0            1477            1477  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)

