test@tesla-t4:~/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2$ /usr/local/cuda-11.4/bin/nsys   profile  --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Time per frame : 342.664 seconds
Time per frame : 354.314 seconds
Time per frame : 348.099 seconds
Time per frame : 343.821 seconds
Time per frame : 346.334 seconds
Time per frame : 346.781 seconds
Time per frame : 339.461 seconds
Time per frame : 338.989 seconds
Time per frame : 343.075 seconds
Time per frame : 346.658 seconds
Time per frame : 352.726 seconds
Time per frame : 360.655 seconds
Time per frame : 359.31 seconds
Time per frame : 353.8 seconds
Time per frame : 358.513 seconds
Time per frame : 358.532 seconds
Time per frame : 355.166 seconds
Time per frame : 362.797 seconds
Time per frame : 350.716 seconds
Time per frame : 349.125 seconds
Time per frame : 343.738 seconds
Time per frame : 341.311 seconds
Time per frame : 336.162 seconds
Time per frame : 338.32 seconds
Time per frame : 335.677 seconds
Time per frame : 334.214 seconds
Time per frame : 331.931 seconds
Time per frame : 330.963 seconds
Time per frame : 346.931 seconds
Time per frame : 352.9 seconds
Time per frame : 352.294 seconds
Time per frame : 357.35 seconds
Time per frame : 361.832 seconds
Time per frame : 354.089 seconds
Time per frame : 354.865 seconds
Time per frame : 359.782 seconds
Time per frame : 359.517 seconds
Time per frame : 357.917 seconds
Time per frame : 357.559 seconds
Time per frame : 357.158 seconds
Time per frame : 352.927 seconds
Time per frame : 349.815 seconds
Time per frame : 352.244 seconds
Time per frame : 358.687 seconds
Time per frame : 360.338 seconds
Time per frame : 361.798 seconds
Time per frame : 357.967 seconds
Time per frame : 356.808 seconds
Time per frame : 360.086 seconds
Time per frame : 359.647 seconds
Time per frame : 348.122 seconds
Time per frame : 349.458 seconds
Time per frame : 360.162 seconds
Time per frame : 362.157 seconds
Time per frame : 361.598 seconds
Time per frame : 361.505 seconds
Time per frame : 358.375 seconds
Time per frame : 357.919 seconds
Time per frame : 361.245 seconds
Time per frame : 354.906 seconds
Time per frame : 354.743 seconds
Time per frame : 349.484 seconds
Time per frame : 349.385 seconds
Time per frame : 349.285 seconds
Time per frame : 342.118 seconds
Time per frame : 344.249 seconds
Time per frame : 343.588 seconds
Time per frame : 353.09 seconds
Time per frame : 384.51 seconds
Time per frame : 377.052 seconds
Time per frame : 373.633 seconds
Time per frame : 369.491 seconds
Time per frame : 369.58 seconds
Time per frame : 369.921 seconds
Time per frame : 362.774 seconds
Time per frame : 347.788 seconds
Time per frame : 345.898 seconds
Time per frame : 345.022 seconds
Time per frame : 341.673 seconds
Time per frame : 349.631 seconds
Time per frame : 353.456 seconds
Time per frame : 349.557 seconds
Time per frame : 353.444 seconds
Time per frame : 350.96 seconds
Time per frame : 354.934 seconds
Time per frame : 357.182 seconds
Time per frame : 352.221 seconds
Time per frame : 355.846 seconds
Time per frame : 364.709 seconds
Time per frame : 361.847 seconds
Time per frame : 363.206 seconds
Time per frame : 358.796 seconds
Time per frame : 361.373 seconds
Time per frame : 353.524 seconds
Time per frame : 348.622 seconds
Time per frame : 346.697 seconds
Time per frame : 343.567 seconds
Time per frame : 348.075 seconds
Time per frame : 353.27 seconds
Time per frame : 350.638 seconds
took 35339.1 seconds.
Processing events...
Saving temporary "/tmp/nsys-report-ca76-b7b5-d404-8aa9.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-ca76-b7b5-d404-8aa9.qdrep"
Exporting 6226611 events: [===============================================100%]

Exported successfully to
/tmp/nsys-report-ca76-b7b5-d404-8aa9.sqlite


CUDA API Statistics:

 Time(%)   Total Time (ns)    Num Calls       Average         Minimum        Maximum           StdDev                Name         
 -------  ------------------  ---------  -----------------  -----------  ---------------  -----------------  ---------------------
   100.0  35,243,186,811,395        202  174,471,221,838.6       32,806  383,581,387,514  176,678,707,277.6  cudaDeviceSynchronize
     0.0         149,913,322          1      149,913,322.0  149,913,322      149,913,322                0.0  cudaMallocManaged    
     0.0          38,428,351          1       38,428,351.0   38,428,351       38,428,351                0.0  cudaDeviceReset      
     0.0          19,663,012          5        3,932,602.4        3,174       16,469,196        7,121,662.0  cudaFree             
     0.0           6,501,159        303           21,456.0        3,081        2,268,326          129,795.3  cudaLaunchKernel     
     0.0             404,295          4          101,073.8        2,122          247,994          119,941.1  cudaMalloc           
     0.0               2,224          1            2,224.0        2,224            2,224                0.0  cuCtxSynchronize     



CUDA Kernel Statistics:

 Time(%)   Total Time (ns)    Instances       Average           Minimum          Maximum          StdDev                                          Name                                   
 -------  ------------------  ---------  -----------------  ---------------  ---------------  ---------------  --------------------------------------------------------------------------
   100.0  35,234,298,983,692        100  352,342,989,836.9  330,131,251,564  383,581,461,062  9,305,685,298.3  render(vec3*, int, int, int, camera**, hitable**, curandStateXORWOW*)     
     0.0       8,864,384,546          1    8,864,384,546.0    8,864,384,546    8,864,384,546              0.0  render_init(int, int, curandStateXORWOW*)                                 
     0.0          17,772,906          1       17,772,906.0       17,772,906       17,772,906              0.0  create_world(hitable**, hitable**, curandStateXORWOW*)                    
     0.0          16,460,244          1       16,460,244.0       16,460,244       16,460,244              0.0  free_world(hitable**, hitable**)                                          
     0.0           4,489,677        100           44,896.8           34,911           53,694          1,773.3  init_cam(camera**, unsigned int, unsigned int, unsigned int, unsigned int)
     0.0           3,746,725        100           37,467.3           34,207           48,926          2,391.2  delete_cam(camera**)                                                      



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum   StdDev               Operation            
 -------  ---------------  ----------  --------  -------  -------  --------  ---------------------------------
    56.5      278,253,291      27,587  10,086.4    1,919  123,100  15,501.5  [CUDA Unified Memory memcpy HtoD]
    43.5      214,327,114      21,400  10,015.3    1,311  111,739  19,341.6  [CUDA Unified Memory memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

     Total      Operations  Average  Minimum   Maximum   StdDev               Operation            
 -------------  ----------  -------  -------  ---------  -------  ---------------------------------
 2,430,000.000      21,400  113.551    4.000  1,012.000  246.480  [CUDA Unified Memory memcpy DtoH]
 2,405,700.000      27,587   87.204    4.000  1,016.000  177.719  [CUDA Unified Memory memcpy HtoD]



Operating System Runtime API Statistics:

 Time(%)   Total Time (ns)    Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ------------------  ---------  ------------  -------  -----------  ------------  --------------
    91.4  64,440,449,043,439  3,245,538  19,855,090.0    1,136  100,536,612  28,000,501.1  poll          
     8.6   6,030,154,711,808  2,893,593   2,083,967.8    3,669   20,939,282      21,660.4  sem_timedwait 
     0.0         503,607,146     36,205      13,909.9    1,000   15,873,357      98,751.7  ioctl         
     0.0           4,240,563         27     157,057.9    1,026    2,933,131     560,402.3  mmap          
     0.0           3,242,583        115      28,196.4    1,641      792,148      85,478.3  mmap64        
     0.0           2,273,177         24      94,715.7    1,023      866,106     185,002.7  fread         
     0.0           1,740,239        141      12,342.1    1,058      499,112      50,264.2  fopen         
     0.0             479,090         88       5,444.2    2,039       18,041       2,056.3  open64        
     0.0             333,693        100       3,336.9    2,716       19,211       1,619.7  putc          
     0.0             296,403         37       8,010.9    1,072      123,633      21,164.4  munmap        
     0.0              61,839          1      61,839.0   61,839       61,839           0.0  pthread_join  
     0.0              52,919          4      13,229.8   11,885       15,887       1,832.1  pthread_create
     0.0              41,605         19       2,189.7    1,042        5,323         952.7  write         
     0.0              24,376          1      24,376.0   24,376       24,376           0.0  fgets         
     0.0              18,443          9       2,049.2    1,047        4,569       1,354.8  fclose        
     0.0              13,891          6       2,315.2    1,367        4,334       1,041.0  open          
     0.0               6,538          2       3,269.0    2,070        4,468       1,695.6  fwrite        
     0.0               5,810          2       2,905.0    2,221        3,589         967.3  fgetc         
     0.0               5,199          3       1,733.0    1,011        2,536         765.7  fcntl         
     0.0               3,712          2       1,856.0    1,288        2,424         803.3  socket        
     0.0               3,364          1       3,364.0    3,364        3,364           0.0  connect       
     0.0               2,882          1       2,882.0    2,882        2,882           0.0  pipe2         
     0.0               1,071          1       1,071.0    1,071        1,071           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report4.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report4.sqlite"

