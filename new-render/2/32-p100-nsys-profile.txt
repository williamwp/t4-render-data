
0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/t4-render-data/new-render/104/b/cuPathTracer-master/report2.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   67.3       601727496          16      37607968.5           20654       100161436  poll                                                                            
   31.8       284214179        4053         70124.4            1146        91436270  ioctl                                                                           
    0.2         2165170          57         37985.4            1565          797701  mmap                                                                            
    0.2         1750741         262          6682.2            5255           16593  writev                                                                          
    0.2         1591736          30         53057.9            1927         1447295  fopen                                                                           
    0.1         1243310          59         21073.1            5780           38732  open64                                                                          
    0.0          426275           9         47363.9           14021           78555  sem_timedwait                                                                   
    0.0          411248           1        411248.0          411248          411248  fopen64                                                                         
    0.0          366947          25         14677.9            1233          314498  fclose                                                                          
    0.0          250934           3         83644.7           79276           90020  fgets                                                                           
    0.0          233953           7         33421.9            3218          167234  fread                                                                           
    0.0           87138           2         43569.0           41747           45391  pthread_create                                                                  
    0.0           56273          11          5115.7            2815            9515  write                                                                           
    0.0           35198          12          2933.2            1614            4236  read                                                                            
    0.0           33792           5          6758.4            1525           10295  fflush                                                                          
    0.0           32004           4          8001.0            4179           14237  open                                                                            
    0.0           23748           6          3958.0            2511            5383  munmap                                                                          
    0.0           14933           8          1866.6            1031            3865  fcntl                                                                           
    0.0           12905           2          6452.5            5720            7185  socket                                                                          
    0.0            9737           1          9737.0            9737            9737  pipe2                                                                           
    0.0            9189           1          9189.0            9189            9189  connect                                                                         
    0.0            3498           1          3498.0            3498            3498  fwrite                                                                          
    0.0            2582           1          2582.0            2582            2582  pthread_cond_broadcast                                                          
    0.0            2553           1          2553.0            2553            2553  bind                                                                            
    0.0            1189           1          1189.0            1189            1189  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)

