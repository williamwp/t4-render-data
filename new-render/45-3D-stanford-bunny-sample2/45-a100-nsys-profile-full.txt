Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-11a2-0e26-86ef-64dc.qdrep"
Exporting 24920 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-11a2-0e26-86ef-64dc.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   63.8       982726774        5725        171655.3             109       967607556  cudaMalloc                                                                      
   31.3       482678645           1     482678645.0       482678645       482678645  cudaDeviceSynchronize                                                           
    3.8        58606955        5609         10448.7             108          443357  cudaMemcpy                                                                      
    0.6         9728509         929         10472.0            2493         6996968  cudaLaunchKernel                                                                
    0.5         7597984        4368          1739.5             248           95425  cudaFree                                                                        





Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.4        11754593        5383          2183.7            2080            3264  [CUDA memcpy HtoD]                                                              
    0.6           69090           1         69090.0           69090           69090  [CUDA memcpy DtoH]                                                              


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
            768.000               1              768.000            768.000              768.000  [CUDA memcpy DtoH]                                                              
            549.766            5383                0.102              0.008                0.391  [CUDA memcpy HtoD]                                                              




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   97.1      4809067574          60      80151126.2           23365       100210585  poll                                                                            
    2.6       129356463        1044        123904.7            1007        19696112  ioctl                                                                           
    0.1         6174809          43        143600.2            1147         5974943  fopen                                                                           
    0.1         2580321          84         30718.1            1112          770341  mmap                                                                            
    0.0         1727014          89         19404.7            5657           36224  open64                                                                          
    0.0         1399276          11        127206.9           14896          699690  sem_timedwait                                                                   
    0.0         1149648          59         19485.6            1084          329333  read                                                                            
    0.0          188418           4         47104.5           36649           54401  pthread_create                                                                  
    0.0          157270           3         52423.3           50728           54924  fgets                                                                           
    0.0           83085          27          3077.2            1017           42420  fclose                                                                          
    0.0           77202          11          7018.4            1244           32768  fread                                                                           
    0.0           47595          12          3966.3            2139            7338  write                                                                           
    0.0           45093           8          5636.6            1181           18741  fwrite                                                                          
    0.0           37286          10          3728.6            1433            7525  munmap                                                                          
    0.0           37119           5          7423.8            3693           12744  open                                                                            
    0.0           22457           4          5614.2            4097            6322  fopen64                                                                         
    0.0           16044           2          8022.0            7659            8385  putc                                                                            
    0.0           12537           5          2507.4            1248            4072  fcntl                                                                           
    0.0           10366           2          5183.0            4426            5940  socket                                                                          
    0.0            8755           1          8755.0            8755            8755  connect                                                                         
    0.0            6344           1          6344.0            6344            6344  pipe2                                                                           
    0.0            2179           2          1089.5            1049            1130  pthread_mutex_trylock                                                           
    0.0            2063           1          2063.0            2063            2063  bind                                                                            
    0.0            1873           1          1873.0            1873            1873  pthread_mutex_lock                                                              




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/45-3D-stanford-bunny-sample2/report2.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/45-3D-stanford-bunny-sample2/report2.sqlite"
