wangpeng@rongtian-SYS-7049GP-TRT:~/backup6/GPU-rendering-programs/45-3D-stanford-bunny-sample2$ /home/chenjiuyi/usr_chenjiuyi/local/cuda-11.1/bin/nsys   profile --stats=true  ./run -s scene4.json
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Using following defaults:
+ Width: 512
+ Height: 512
+ MaxRayDepth: 3
+ Resources: resources.json
+ Image: image.png
Oct taken seconds: 9.67405
Rendering seconds: 0.860631
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-6976-2496-37cf-59da.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-6976-2496-37cf-59da.qdrep"
Exporting 66111 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-6976-2496-37cf-59da.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   73.8      2644721941       10818        244474.2             108      1979346372  cudaMalloc                                                                      
   21.7       778465822           1     778465822.0       778465822       778465822  cudaDeviceSynchronize                                                           
    3.0       107837110       10538         10233.2             106          468955  cudaMemcpy                                                                      
    1.0        36629633       29780          1230.0             245         1184930  cudaFree                                                                        
    0.4        14149245        2241          6313.8            2551         7030883  cudaLaunchKernel                                                                





Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.7        21311757        9775          2180.2            2080            3233  [CUDA memcpy HtoD]                                                              
    0.3           68641           1         68641.0           68641           68641  [CUDA memcpy DtoH]                                                              


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
            958.203            9775                0.098              0.008                0.391  [CUDA memcpy HtoD]                                                              
            768.000               1              768.000            768.000              768.000  [CUDA memcpy DtoH]                                                              




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   87.0     11922322305         131      91010093.9           23584       100213837  poll                                                                            
   12.9      1769356122        1046       1691545.0            1060       853606158  ioctl                                                                           
    0.0         5267261          41        128469.8            1000         5092457  fopen                                                                           
    0.0         2287784          84         27235.5            1083          767949  mmap                                                                            
    0.0         2061918          89         23167.6            3233          407332  open64                                                                          
    0.0         1441038          11        131003.5           15379          710880  sem_timedwait                                                                   
    0.0          150605           4         37651.3           31956           50939  pthread_create                                                                  
    0.0          123530          42          2941.2            1038            6015  read                                                                            
    0.0           88324           3         29441.3           26590           34194  fgets                                                                           
    0.0           74512          17          4383.1            1120           36412  fclose                                                                          
    0.0           64510          10          6451.0            1409           25998  fread                                                                           
    0.0           50671          12          4222.6            2211            6121  write                                                                           
    0.0           39282           7          5611.7            1621           17306  fwrite                                                                          
    0.0           39137          11          3557.9            1182            9020  munmap                                                                          
    0.0           28320           5          5664.0            2177           10046  open                                                                            
    0.0           20747           4          5186.7            2789            6384  fopen64                                                                         
    0.0           13059           2          6529.5            6453            6606  putc                                                                            
    0.0           12066           1         12066.0           12066           12066  pthread_mutex_lock                                                              
    0.0            9817           4          2454.3            1096            3555  fcntl                                                                           
    0.0            6955           2          3477.5            2951            4004  socket                                                                          
    0.0            6279           1          6279.0            6279            6279  pipe2                                                                           
    0.0            4483           1          4483.0            4483            4483  connect                                                                         
    0.0            1085           1          1085.0            1085            1085  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/45-3D-stanford-bunny-sample2/report7.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/45-3D-stanford-bunny-sample2/report7.sqlite"

