88== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: plot_pixel(unsigned char*, comp, double)
       5485                             inst_per_warp                                                 Instructions per warp  2.1234e+03  4.9441e+04  1.2462e+04
       5485                         branch_efficiency                                                     Branch Efficiency      99.43%     100.00%      99.99%
       5485                 warp_execution_efficiency                                             Warp Execution Efficiency      53.94%     100.00%      99.35%
       5485         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      52.17%      96.40%      95.77%
       5485                      inst_replay_overhead                                           Instruction Replay Overhead    0.000017    0.000148    0.000031
       5485      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
       5485     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
       5485       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
       5485      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
       5485              gld_transactions_per_request                                  Global Load Transactions Per Request    0.000000    0.000000    0.000000
       5485              gst_transactions_per_request                                 Global Store Transactions Per Request    3.000000    5.337407    3.002465
       5485                 shared_store_transactions                                             Shared Store Transactions           0           0           0
       5485                  shared_load_transactions                                              Shared Load Transactions           0           0           0
       5485                   local_load_transactions                                               Local Load Transactions           0      639436         686
       5485                  local_store_transactions                                              Local Store Transactions           0      489702         433
       5485                          gld_transactions                                              Global Load Transactions           2     1544322        1016
       5485                          gst_transactions                                             Global Store Transactions      583200     1037592      583679
       5485                  sysmem_read_transactions                                       System Memory Read Transactions           0        7279           5
       5485                 sysmem_write_transactions                                      System Memory Write Transactions           5        7135          10
       5485                      l2_read_transactions                                                  L2 Read Transactions          96      802150        1894
       5485                     l2_write_transactions                                                 L2 Write Transactions      583213     1575440      583749
       5485                    dram_read_transactions                                       Device Memory Read Transactions           5      388438         775
       5485                   dram_write_transactions                                      Device Memory Write Transactions      164577      676855      194591
       5485                           global_hit_rate                                     Global Hit Rate in unified l1/tex       0.00%      26.66%       0.03%
       5485                            local_hit_rate                                                        Local Hit Rate       0.00%      48.68%       0.05%
       5485                  gld_requested_throughput                                      Requested Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       5485                  gst_requested_throughput                                     Requested Global Store Throughput  198.50MB/s  4.1758GB/s  792.27MB/s
       5485                            gld_throughput                                                Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       5485                            gst_throughput                                               Global Store Throughput  595.49MB/s  12.527GB/s  2.3230GB/s
       5485                     local_memory_overhead                                                 Local Memory Overhead       0.00%      35.48%       0.06%
       5485                        tex_cache_hit_rate                                                Unified Cache Hit Rate       1.63%      68.14%      62.42%
       5478                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)       0.00%       0.00%       0.00%
       5485                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      31.77%      98.42%      83.27%
       5485                      dram_read_throughput                                         Device Memory Read Throughput  20.601KB/s  1.5498GB/s  3.1586MB/s
       5485                     dram_write_throughput                                        Device Memory Write Throughput  202.99MB/s  4.3020GB/s  793.05MB/s
       5485                      tex_cache_throughput                                              Unified Cache Throughput  0.00000B/s  3.7967GB/s  4.1322MB/s
       5485                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  0.00000B/s  3.1785GB/s  4.0582MB/s
       5485                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  595.49MB/s  12.527GB/s  2.3264GB/s
       5485                        l2_read_throughput                                                 L2 Throughput (Reads)  147.80KB/s  3.2015GB/s  7.7207MB/s
       5485                       l2_write_throughput                                                L2 Throughput (Writes)  595.50MB/s  12.528GB/s  2.3233GB/s
       5485                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  29.740MB/s  21.349KB/s
       5485                   sysmem_write_throughput                                        System Memory Write Throughput  5.2275KB/s  29.143MB/s  42.330KB/s
       5485                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  2.5479GB/s  2.7976MB/s
       5485                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  1.9527GB/s  1.7654MB/s
       5485                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       5485                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       5485                            gld_efficiency                                         Global Memory Load Efficiency       0.00%       0.00%       0.00%
       5485                            gst_efficiency                                        Global Memory Store Efficiency      18.74%      33.33%      33.32%
       5485                    tex_cache_transactions                                            Unified Cache Transactions           0      951686        1013
       5485                             flop_count_dp                           Floating Point Operations(Double Precision)  2009534700  7.6055e+10  1.9579e+10
       5485                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)   275512061  1.0852e+10  2785438000
       5485                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)   590423069  2.1747e+10  5610298516
       5485                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)   553176501  2.1709e+10  5573014665
       5485                             flop_count_sp                           Floating Point Operations(Single Precision)    24883200    24883200    24883200
       5485                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)           0           0           0
       5485                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)    12441600    12441600    12441600
       5485                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)           0           0           0
       5485                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   100131848  3626178772   936777820
       5485                             inst_executed                                                 Instructions Executed   137598369  3203744843   807537013
       5485                               inst_issued                                                   Instructions Issued   137609975  3203800651   807561407
       5485                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
       5485                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
       5485                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)       4.97%       6.36%       5.08%
       5485                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)       5.23%       7.03%       5.35%
       5485                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       0.00%       0.00%       0.00%
       5485                             stall_texture                                         Issue Stall Reasons (Texture)       0.00%       0.00%       0.00%
       5485                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
       5485                               stall_other                                           Issue Stall Reasons (Other)      79.55%      83.88%      83.54%
       5485          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.00%       0.06%       0.00%
       5485                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       3.49%       4.22%       3.56%
       5485                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
       5485                                inst_fp_32                                               FP Instructions(Single)   133309448  3659356372   969955420
       5485                                inst_fp_64                                               FP Instructions(Double)  1508875724  5.7926e+10  1.4895e+10
       5485                              inst_integer                                                  Integer Instructions   468250481  1.5111e+10  3921481859
       5485                          inst_bit_convert                                              Bit-Convert Instructions    18662400    18662400    18662400
       5485                              inst_control                                             Control-Flow Instructions   223164534  7733917141  2000202433
       5485                        inst_compute_ld_st                                               Load/Store Instructions     6220800     6220800     6220800
       5485                                 inst_misc                                                     Misc Instructions   383078226  1.1443e+10  2993497734
       5485           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
       5485                               issue_slots                                                           Issue Slots   126412051  2954445625   744453304
       5485                                 cf_issued                                      Issued Control-Flow Instructions    15410718   365726414    91855618
       5485                               cf_executed                                    Executed Control-Flow Instructions    15410718   365726414    91855618
       5485                               ldst_issued                                        Issued Load/Store Instructions     1036800     1036800     1036800
       5485                             ldst_executed                                      Executed Load/Store Instructions      453600      453600      453600
       5485                       atomic_transactions                                                   Atomic Transactions           0       10638           6
       5485           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
       5485                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  45.107MB/s  14.443KB/s
       5485                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0       22094           6
       5485                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)           0      797118         995
       5485                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.03%       0.01%
       5485                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       2.41%       2.95%       2.46%
       5485                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)      583200     1529898      584534
       5485                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
       5485                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
       5485                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
       5485                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
       5485                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
       5485                                       ipc                                                          Executed IPC    1.382494    1.477961    1.467640
       5485                                issued_ipc                                                            Issued IPC    1.382634    1.478079    1.467681
       5485                    issue_slot_utilization                                                Issue Slot Utilization      63.59%      68.01%      67.65%
       5485                             sm_efficiency                                               Multiprocessor Activity      88.55%      99.64%      98.80%
       5485                        achieved_occupancy                                                    Achieved Occupancy    0.701054    0.963222    0.945344
       5485                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    2.557491    2.811589    2.809035
       5485                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
       5485                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
       5485                           tex_utilization                                             Unified Cache Utilization    Idle (0)     Low (1)    Idle (0)
       5485                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
       5485                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
       5485                        tex_fu_utilization                                     Texture Function Unit Utilization     Low (1)     Low (1)     Low (1)
       5485                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (2)     Low (1)
       5485             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
       5485           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
       5485           double_precision_fu_utilization                            Double-Precision Function Unit Utilization    High (8)    High (8)    High (8)
       5485                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
       5485                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       0.01%       0.19%       0.04%
       5485                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)      24.78%      55.66%      54.80%
       5485                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)     Low (1)    Idle (0)
       5485                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
       5485               pcie_total_data_transmitted                                           PCIe Total Data Transmitted           0       53760        2427
       5485                  pcie_total_data_received                                              PCIe Total Data Received           0       43008         153
       5485                inst_executed_global_loads                              Warp level instructions for global loads           0           0           0
       5485                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
       5485                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
       5485               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
       5485               inst_executed_global_stores                             Warp level instructions for global stores      194400      194400      194400
       5485                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
       5485               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
       5485              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
       5485              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
       5485           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
       5485             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
       5485          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
       5485              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
       5485                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
       5485                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads           0    13883008        9879
       5485                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0    10030592        9285
       5485                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
       5485                           dram_read_bytes                                Total bytes read from DRAM to L2 cache         160    12430016       24800
       5485                          dram_write_bytes                             Total bytes written from L2 cache to DRAM     5266464    21659360     6226916
       5485               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.    18662400    34332864    18691594
       5485                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0       31744          21
       5485              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0      344064         252
       5485                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
       5485                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
       5485             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
       5485                      global_load_requests              Total number of global load requests from Multiprocessor           0      291944          53
       5485                       local_load_requests               Total number of local load requests from Multiprocessor           0      325810         118
       5485                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
       5485                     global_store_requests             Total number of global store requests from Multiprocessor      777600     1274510      778127
       5485                      local_store_requests              Total number of local store requests from Multiprocessor           0      478196         173
       5485                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
       5485                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0       10638           5
       5485                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0         922           0
       5485                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
       5485                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
       5485                         sysmem_read_bytes                                              System Memory Read Bytes           0      232928         163
       5485                        sysmem_write_bytes                                             System Memory Write Bytes         160      228320         324
       5485                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      31.77%      98.42%      83.27%
       5485                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
       5485                     unique_warps_launched                                              Number of warps launched       64800       64800       64800
==46688== Warning: One or more events or metrics can't be profiled. Rerun with "--print-gpu-trace" for detail.

