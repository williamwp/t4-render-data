#include <stdint.h>
#include <stdio.h>

#include "utils/utils.h"

extern "C" __device__ __noinline__ void count_instrs(int predicate,
                                                     int count_warp_level,
                                                     uint64_t pcounter) {
    /* all the active threads will compute the active mask (ballot() is
     * implemented in utils/utils.h)*/
    const int active_mask = ballot(1);

    /* compute the predicate mask */
    const int predicate_mask = ballot(predicate);

    /* each thread will get a lane id (get_lane_id is implemented in
     * utils/utils.h) */
    const int laneid = get_laneid();

    /* get the id of the first active thread */
    const int first_laneid = __ffs(active_mask) - 1;

    /* count all the active thread */
    const int num_threads = __popc(predicate_mask);

    /* only the first active thread will perform the atomic */
    if (first_laneid == laneid) {
        if (count_warp_level) {
            /* num threads can be zero when accounting for predicates off */
            if (num_threads > 0) {
                atomicAdd((unsigned long long *)pcounter, 1);
            }
        } else {
            atomicAdd((unsigned long long *)pcounter, num_threads);
        }
    }
}
