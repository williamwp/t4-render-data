
Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-c8a4-7ac8-65e5-e69c.qdrep"
Exporting 4701 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-c8a4-7ac8-65e5-e69c.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls     Average       Minimum       Maximum        StdDev              Name         
 -------  ---------------  ---------  -------------  -----------  -------------  -------------  ---------------------
    62.0    5,354,015,265      1,025    5,223,429.5       10,975  5,341,846,637  166,850,885.8  cudaMemcpy           
    30.9    2,671,711,326         15  178,114,088.4  175,462,206    181,189,338    2,087,290.0  cudaDeviceSynchronize
     7.0      605,432,986          4  151,358,246.5       77,708    604,617,214  302,172,763.5  cudaMalloc           
     0.0        3,536,777         49       72,179.1        2,038      3,041,089      433,073.0  cudaLaunchKernel     
     0.0        1,510,261          4      377,565.3       84,689        856,802      356,716.9  cudaFree             
     0.0           19,639          1       19,639.0       19,639         19,639            0.0  cudaMemset           



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances     Average      Minimum       Maximum         StdDev                                                       Name                                                
 -------  ---------------  ---------  -------------  ----------  -------------  ---------------  ----------------------------------------------------------------------------------------------------
    83.2    6,667,855,407         16  416,740,962.9  86,275,764  5,340,748,988  1,313,069,595.4  void rtRaymarch_cukern<CubeSquareFractal, true>(Ray*, int, int, CubeSquareFractal, int)             
    16.8    1,344,671,924         15   89,644,794.9  88,595,445     90,824,121        711,611.1  void initRayBufferWithDepthCache_cukern<true>(Ray*, int, int, RayPtr*, int, int, mat4x4)            
     0.0        1,773,744          1    1,773,744.0   1,773,744      1,773,744              0.0  renderDepthCache_cukern(RayPtr*, int*, Ray const*, int)                                             
     0.0          814,602          1      814,602.0     814,602        814,602              0.0  void initRayBuffer_cukern<false>(Ray*, int, int, int, mat4x4)                                       
     0.0          175,353         15       11,690.2      11,327         12,063            211.4  void shaderNormal_cukern<(RenderActionType)2, CubeSquareFractal, IndexedRayLookup, DefaultEnvMap>(v…
     0.0          132,284          1      132,284.0     132,284        132,284              0.0  AXPY_cukern(float*, int, float, float)                                                              



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average   Minimum  Maximum  StdDev      Operation     
 -------  ---------------  ----------  --------  -------  -------  ------  ------------------
    98.4        2,379,715       1,025   2,321.7    1,440   12,096   515.2  [CUDA memcpy DtoH]
     1.6           38,623           1  38,623.0   38,623   38,623     0.0  [CUDA memset]     



CUDA Memory Operation Statistics (by size in KiB):

   Total     Operations   Average    Minimum    Maximum   StdDev      Operation     
 ----------  ----------  ---------  ---------  ---------  ------  ------------------
  8,192.000           1  8,192.000  8,192.000  8,192.000   0.000  [CUDA memset]     
 16,384.004       1,025     15.984      0.004     16.000   0.500  [CUDA memcpy DtoH]



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    99.4    8,611,562,328         97  88,778,993.1   12,565  100,141,804  30,853,104.8  poll          
     0.6       49,965,727        685      72,942.7    1,000   12,702,614     567,579.7  ioctl         
     0.0        1,932,438      1,024       1,887.1    1,623       18,792         924.8  writev        
     0.0        1,328,535         68      19,537.3    1,627      461,135      54,904.7  mmap64        
     0.0          555,573         10      55,557.3    9,746      168,844      43,620.5  sem_timedwait 
     0.0          481,552         27      17,835.3    1,041      413,795      79,156.4  fopen         
     0.0          466,115         86       5,419.9    1,855       11,988       1,608.1  open64        
     0.0          256,146          3      85,382.0    1,246      197,134     100,821.7  fwrite        
     0.0          239,183          5      47,836.6    1,010      232,157     103,041.1  fclose        
     0.0           99,295          1      99,295.0   99,295       99,295           0.0  fopen64       
     0.0           64,303         15       4,286.9    1,002       20,900       4,835.1  mmap          
     0.0           52,114          4      13,028.5   11,653       14,880       1,598.5  pthread_create
     0.0           40,962          5       8,192.4    2,048       16,303       6,599.4  fgetc         
     0.0           30,292         18       1,682.9    1,235        3,741         601.4  fflush        
     0.0           24,796          1      24,796.0   24,796       24,796           0.0  fgets         
     0.0           23,720          4       5,930.0    1,357       18,650       8,489.1  fread         
     0.0           21,747         11       1,977.0    1,040        4,482         932.2  munmap        
     0.0           21,455          9       2,383.9    1,099        4,394       1,074.5  write         
     0.0           14,281          6       2,380.2    1,453        4,131         920.3  open          
     0.0            4,158          2       2,079.0    1,950        2,208         182.4  fcntl         
     0.0            3,732          2       1,866.0    1,586        2,146         396.0  socket        
     0.0            3,407          1       3,407.0    3,407        3,407           0.0  pipe2         
     0.0            3,209          1       3,209.0    3,209        3,209           0.0  connect       
     0.0            2,291          2       1,145.5    1,124        1,167          30.4  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/39-Cuda-mandel3d-sample2/report4.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/39-Cuda-mandel3d-sample2/report4.sqlite"
