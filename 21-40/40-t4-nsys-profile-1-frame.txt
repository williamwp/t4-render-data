test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2$ /usr/local/cuda-11.4/bin/nsys  profile --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
^CSignal 2 (SIGINT) was forwarded to the target application process group 10664

The target application terminated with signal 2 (SIGINT)
Processing events...
Saving temporary "/tmp/nsys-report-af97-6a6e-1257-f780.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-af97-6a6e-1257-f780.qdrep"
Exporting 54823 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-af97-6a6e-1257-f780.sqlite


Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    91.5  557,607,548,421     28,089  19,851,456.0   12,105  100,168,369  28,007,155.6  poll          
     8.5   52,035,877,357     25,039   2,078,193.1    9,239   21,077,780     127,236.3  sem_timedwait 
     0.0       49,199,064        966      50,930.7    1,002    7,248,465     329,188.3  ioctl         
     0.0        1,274,466         68      18,742.1    1,556      444,346      52,879.0  mmap64        
     0.0          690,721         50      13,814.4    1,046      534,217      75,120.6  fopen         
     0.0          677,560         16      42,347.5    1,021      119,167      38,437.7  fwrite        
     0.0          439,251         86       5,107.6    1,816       11,647       1,382.1  open64        
     0.0           73,895         17       4,346.8    1,178       20,482       4,449.8  mmap          
     0.0           49,590          4      12,397.5   10,690       14,938       1,880.0  pthread_create
     0.0           36,449         13       2,803.8    1,258        6,490       1,667.2  fread         
     0.0           31,480          9       3,497.8    1,164        7,901       2,764.9  munmap        
     0.0           26,543         14       1,895.9    1,038        2,701         572.5  fclose        
     0.0           24,149          1      24,149.0   24,149       24,149           0.0  fgets         
     0.0           21,390          9       2,376.7    1,030        4,506       1,198.4  write         
     0.0           14,421          6       2,403.5    1,373        4,467       1,088.2  open          
     0.0            9,812          6       1,635.3    1,431        2,281         327.5  fflush        
     0.0            9,192          7       1,313.1    1,009        1,679         285.7  fcntl         
     0.0            6,621          2       3,310.5    2,213        4,408       1,552.1  fgetc         
     0.0            3,948          2       1,974.0    1,611        2,337         513.4  socket        
     0.0            3,164          1       3,164.0    3,164        3,164           0.0  connect       
     0.0            2,746          1       2,746.0    2,746        2,746           0.0  pipe2         
     0.0            1,140          1       1,140.0    1,140        1,140           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report7.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/40-Cuda-ray-shadows-sample2/report7.sqlite"
