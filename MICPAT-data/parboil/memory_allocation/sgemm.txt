wangpeng@dacent:~/tools/gitlab-william/renderbench/parboil-benchmark/benchmarks/sgemm/build/cuda_default$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/memory_allocation/memory_allocation.so  ./sgemm.x -i matrix1.txt,matrix2t.txt,matrix2t.txt -o a.tx
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Opening file:matrix1.txt
Matrix dimension: 1024x992
Opening file:matrix2t.txt
Matrix dimension: 1056x992
Found an allocation!
0x7f7e93000000, 4063232 
Found an allocation!
0x7f7e93400000, 4190208 
Found an allocation!
0x7f7e93800000, 4325376 
Encountered a memcpy HtoD!
0x7f7e93000000, 0x7f7ebf367010, 4063232 
Encountered a memcpy HtoD!
0x7f7e93000000, 0x7f7ebf367010, 4063232 
Encountered a memcpy HtoD!
0x7f7e93400000, 0x7f7ebef66010, 4190208 
Encountered a memcpy HtoD!
0x7f7e93400000, 0x7f7ebef66010, 4190208 
Opening file:a.tx for write.
Matrix dimension: 1024x1056
GFLOPs = 3.20606
IO        : 1.456243
Kernel    : 0.669166
Copy      : 0.003190
Driver    : 0.152278
Compute   : 0.616104
CPU/Kernel Overlap: 0.152310
Timer Wall Time: 2.744806
Found a free!
0x7f7e93000000
Address: 0x7f7e93800000, Number: 2, Size: 4325376
Address: 0x7f7e93400000, Number: 1, Size: 4190208
Found a free!
0x7f7e93400000
Address: 0x7f7e93800000, Number: 2, Size: 4325376
Found a free!
0x7f7e93800000

