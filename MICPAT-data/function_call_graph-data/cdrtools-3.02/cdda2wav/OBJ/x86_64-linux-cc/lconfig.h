/* lconfig.h.  Generated automatically by configure.  */
#if 0
/* @(#)lconfig.h.in	1.12 15/12/14 Copyright 1998-2003,2015 Heiko Eissfeldt, Copyright 2006-2009 J. Schilling */
#endif
/* #undef HAVE_SYS_CDIO_H */		/* if we should use sys/cdio.h */

/* #undef HAVE_SYS_CDRIO_H */		/* if we should use sys/cdrio.h */

/* #undef HAVE_SUNDEV_SRREG_H */	/* if we should use sundev/srreg.h */

/* #undef HAVE_SYS_AUDIOIO_H */	/* if we should use sys/audioio.h */

/* #undef HAVE_SUN_AUDIOIO_H */	/* if we should use sun/audioio.h */

/* #undef HAVE_SOUNDCARD_H */		/* if we should use soundcard.h */

#define HAVE_SYS_SOUNDCARD_H 1	/* if we should use sys/soundcard.h */

#define HAVE_LINUX_SOUNDCARD_H 1	/* if we should use linux/soundcard.h */

/* #undef HAVE_MACHINE_SOUNDCARD_H */	/* if we should use machine/soundcard.h */

#define HAVE_SNDIO_H 1	/* if we should use sndio.h */

#define HAVE_ALSA_ASOUNDLIB_H 1	/* if we should use alsa/asoundlib.h */

/* #undef HAVE_SYS_ASOUNDLIB_H */	/* if we should use sys/asoundlib.h */

/* #undef HAVE_MMSYSTEM_H */		/* if we should use mmsystem.h */

#define HAVE_PULSE_PULSEAUDIO_H 1	/* if we should use pulse/pulseaudio.h */

#define HAVE_PULSE_SIMPLE_H 1	/* if we should use pulse/simple.h */
