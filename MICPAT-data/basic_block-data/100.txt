wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/100-blur-pictures$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/basic_block/basic_block.so  ./blurCUDA 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------

Give the number of times to blur the image
100
Reading input file: 0.127487 seconds elapsed
Allocation of device memory: 0.675425 seconds elapsed
inspecting ProcessBlurKernel(int*, int*, int*, int*, int*, int*) - number basic blocks 20
inspecting doCopyKernel(int*, int*, int*, int*, int*, int*) - number basic blocks 3

