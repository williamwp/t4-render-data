wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/6-CUDA-with-OpenGL-ray-tracer-sample1$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/basic_block/basic_block.so   ./a.out 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
x,y,z: 0.000000, 0.000000, -20.000000, radius: 4.000000
x,y,z: 0.000000, 20.000000, -30.000000, radius: 3.000000
x,y,z: 0.000000, -10004.000000, -20.000000, radius: 10000.000000
x,y,z: 5.000000, -1.000000, -15.000000, radius: 2.000000
x,y,z: 5.000000, 0.000000, -25.000000, radius: 3.000000
x,y,z: -5.500000, 0.000000, -15.000000, radius: 3.000000
inspecting tracer_kernel(float3*) - number basic blocks 537
took 0.356155 seconds.
