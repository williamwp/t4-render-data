  1 ------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
  2 NVBit core environment variables (mostly for nvbit-devs):
  3            NOINSPECT = 0 - if set, skips function inspection and instrumentation
  4             NVDISASM = nvdisasm - override default nvdisasm found in PATH
  5             NOBANNER = 0 - if set, does not print this banner
  6 ---------------------------------------------------------------------------------
  7         KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
  8           KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
  9     COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
 10     EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
 11         TOOL_VERBOSE = 0 - Enable verbosity inside the tool
 12 ----------------------------------------------------------------------------------------------------
 13 inspecting getColor(int*, Complex*, Complex*) - number basic blocks 33
 14 0
 15 0
 16 0
 17 0

