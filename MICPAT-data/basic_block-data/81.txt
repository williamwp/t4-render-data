wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/81-Luminous-sphere-rendered-sample3/build$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/basic_block/basic_block.so  ./raytracing 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
inspecting generate_seed_pseudo(unsigned long long, unsigned long long, unsigned long long, curandOrdering, curandStateXORWOW*, unsigned int*) - number basic blocks 132
inspecting void gen_sequenced<curandStateXORWOW, float, int, &(float curand_uniform_noargs<curandStateXORWOW>(curandStateXORWOW*, int)), rng_config<curandStateXORWOW> >(curandStateXORWOW*, float*, unsigned long, unsigned long, int) - number basic blocks 12
INFO  -> Rendering
inspecting RayTracer::RenderScreen(CUDA::device_ptr<Sphere>, CUDA::device_ptr<float>, CUDA::device_ptr<glm::vec<3, float, (glm::qualifier)0> >, int, int) - number basic blocks 258
INFO  -> Exporting image to PNG
INFO  -> Done!
took 85.429 seconds.

