     1 ------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
     2 NVBit core environment variables (mostly for nvbit-devs):
     3            NOINSPECT = 0 - if set, skips function inspection and instrumentation
     4             NVDISASM = nvdisasm - override default nvdisasm found in PATH
     5             NOBANNER = 0 - if set, does not print this banner
     6 ---------------------------------------------------------------------------------
     7         KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
     8           KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
     9     COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    10     EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
    11         TOOL_VERBOSE = 0 - Enable verbosity inside the tool
    12 ----------------------------------------------------------------------------------------------------
    13 inspecting render(vec3*, int, int, vec3, vec3, vec3, vec3) - number basic blocks 37
    14 P3
    15 1200 600
    16 255
    17 165 165 165
    18 165 165 165
    19 165 165 165

