------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
thread block size of kernel = 128 
Configuration used: boxes1d = 10
cuDevicePrimaryCtxRetain
cuCtxGetCurrent
cuCtxGetCurrent
cuCtxGetDevice
cuCtxGetDevice
cuModuleGetFunction
cuModuleGetFunction
cuCtxSynchronize
cuCtxSynchronize
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuCtxSynchronize
cuCtxSynchronize
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
Time spent in different stages of GPU_CUDA KERNEL:
 0.652773022652 s, 80.315589904785 % : GPU: SET DEVICE / DRIVER INIT
 0.000539999979 s,  0.066440276802 % : GPU MEM: ALO
 0.001319000032 s,  0.162286534905 % : GPU MEM: COPY IN
 0.155033007264 s, 19.074880599976 % : GPU: KERNEL
 0.002307000104 s,  0.283847630024 % : GPU MEM: COPY OUT
 0.000788000005 s,  0.096953593194 % : GPU MEM: FRE
Total time:
0.812759995461 s


END OF APPLICATION
The inherent APIs of API_function_call is as following:
cuDevicePrimaryCtxRetain
cuCtxGetCurrent
cuCtxGetCurrent
cuCtxGetDevice
cuCtxGetDevice
cuModuleGetFunction
cuModuleGetFunction
cuLaunchKernel
cuLaunchKernel
