------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Reading File
Read File
cuDevicePrimaryCtxRetain
cuCtxGetCurrent
cuCtxGetCurrent
cuCtxGetDevice
cuCtxGetDevice
cuModuleGetFunction
cuModuleGetFunction
cuModuleGetFunction
cuModuleGetFunction
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuMemAlloc_v2
cuMemAlloc_v2
Copied Everything to GPU memory
Start traversing the tree
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
cuMemcpyHtoD_v2
cuMemcpyHtoD_v2
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuLaunchKernel
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
Kernel Executed 12 times
cuMemcpyDtoH_v2
cuMemcpyDtoH_v2
Result stored in result.txt
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2
cuMemFree_v2


END OF APPLICATION
The inherent APIs of API_function_call is as following:
cuDevicePrimaryCtxRetain
cuCtxGetCurrent
cuCtxGetCurrent
cuCtxGetDevice
cuCtxGetDevice
cuModuleGetFunction
cuModuleGetFunction
cuLaunchKernel
cuLaunchKernel
