wangpeng@dacent:~/tools/Rodinia/cuda/backprop$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/block_size/block_size.so ./run 
/usr/share/modules/init/bash: line 36: eval: --: invalid option
eval: usage: eval [arg ...]
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Random number generator seed: 7
Input layer size : 65536
Starting training kernel
Performing GPU computation
Kernel = bpnn_layerforward_CUDA(float*, float*, float*, float*, int, int)
grid X Y Z = 1, 4096, 1
block X Y Z = 16, 16, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 1088
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = bpnn_adjust_weights_cuda(float*, int, float*, int, float*, float*)
grid X Y Z = 1, 4096, 1
block X Y Z = 16, 16, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Training done

