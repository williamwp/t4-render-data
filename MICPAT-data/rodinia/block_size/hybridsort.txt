------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Sorting list of 4194304 floats
Sorting on GPU...Kernel = histogram1024Kernel(unsigned int*, float*, float, float, int)
grid X Y Z = 64, 1, 1
block X Y Z = 96, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 96
shared memory = 12288
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = bucketcount(float*, int*, unsigned int*, int)
grid X Y Z = 1024, 1, 1
block X Y Z = 32, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 32
shared memory = 4096
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = bucketprefixoffset(unsigned int*, unsigned int*, int)
grid X Y Z = 8, 1, 1
block X Y Z = 128, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 128
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = bucketsort(float*, int*, float*, int, unsigned int*, unsigned int*)
grid X Y Z = 1024, 1, 1
block X Y Z = 32, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 32
shared memory = 4096
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortFirst(float4*, int)
grid X Y Z = 4098, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 2585, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 1295, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 650, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 325, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 163, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 84, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 45, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 25, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = mergeSortPass(float4*, int, int)
grid X Y Z = 15, 1, 1
block X Y Z = 208, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 208
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
