------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
WG size of kernel = 16 X 16
Generate input matrix internally, size =256
Creating matrix internally size=256
Before LUD
Found an allocation!
0x7fcb5d000000, 262144 
Encountered a memcpy HtoD!
0x7fcb5d000000, 0x7fcb89a94010, 262144 
Encountered a memcpy HtoD!
0x7fcb5d000000, 0x7fcb89a94010, 262144 
Time consumed(ms): 777.775000
Found a free!
0x7fcb5d000000
After LUD
>>>Verify<<<<
