wangpeng@dacent:~/tools/Rodinia/cuda/leukocyte$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/basic_block/basic_block.so  ./run
/usr/share/modules/init/bash: line 36: eval: --: invalid option
eval: usage: eval [arg ...]
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Detecting cells in frame 0
inspecting GICOV_kernel(int, float*) - number basic blocks 37
inspecting dilate_kernel(int, int, int, int, float*) - number basic blocks 23
Cells detected: 36

Detection runtime
-----------------
GICOV computation: 0.29318 seconds
   GICOV dilation: 0.16417 seconds
            Total: 0.50024 seconds

Tracking cells across 5 frames
Processing frame 1 / 5inspecting IMGVF_kernel(float**, float**, int*, int*, float, float, float, int, float) - number basic blocks 175
Processing frame 5 / 5

Tracking runtime (average per frame):
------------------------------------
MGVF computation: 0.74720 seconds
 Snake evolution: 0.00331 seconds
           Total: 0.75484 seconds

Total application run time: 4.27444 seconds

