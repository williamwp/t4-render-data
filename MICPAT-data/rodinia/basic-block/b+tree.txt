wangpeng@dacent:~/tools/Rodinia/cuda/b+tree$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/basic_block/basic_block.so  ./run 
/usr/share/modules/init/bash: line 36: eval: --: invalid option
eval: usage: eval [arg ...]
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
WG size of kernel 1 & 2  = 256 
Selecting device 0
Input File: ../../data/b+tree/mil.txt 
Command File: ../../data/b+tree/command.txt 
Command Buffer: 
j 6000 3000
k 10000


Getting input from file ../../data/b+tree/mil.txt...
Transforming data to a GPU suitable structure...
Tree transformation took 0.699326
Waiting for command
> 
******command: j count=6000, rSize=6000 
knodes_elem=7874, knodes_unit_mem=2068, knodes_mem=16283432
# of blocks = 6000, # of threads/block = 256 (ensure that device can handle)
inspecting findRangeK - number basic blocks 70
Time spent in different stages of GPU_CUDA KERNEL:
 0.563390016556 s, 74.612197875977 % : GPU: SET DEVICE / DRIVER INIT
 0.000966000021 s,  0.127931609750 % : GPU MEM: ALO
 0.002999000018 s,  0.397170662880 % : GPU MEM: COPY IN
 0.186671003699 s, 24.721656799316 % : GPU: KERNEL
 0.000082999999 s,  0.010992052965 % : GPU MEM: COPY OUT
 0.000982000027 s,  0.130050554872 % : GPU MEM: FRE
Total time:
0.755091011524 s
> > > > > > > > > > > > 
 ******command: k count=10000 
records_elem=1000000, records_unit_mem=4, records_mem=4000000
knodes_elem=7874, knodes_unit_mem=2068, knodes_mem=16283432
# of blocks = 10000, # of threads/block = 256 (ensure that device can handle)
inspecting findK - number basic blocks 49
Time spent in different stages of GPU_CUDA KERNEL:
 0.000012000000 s,  0.006139583420 % : GPU: SET DEVICE / DRIVER INIT
 0.000507000019 s,  0.259397387505 % : GPU MEM: ALO
 0.003320999909 s,  1.699129700661 % : GPU MEM: COPY IN
 0.190406993032 s, 97.418304443359 % : GPU: KERNEL
 0.000054000000 s,  0.027628127486 % : GPU MEM: COPY OUT
 0.001152000041 s,  0.589399993420 % : GPU MEM: FRE
Total time:
0.195453003049 s
> > > > > > > > > > 
