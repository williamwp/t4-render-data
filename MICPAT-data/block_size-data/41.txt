------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
width = 1200
Frame 0/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 460.48
Frame 1/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 164.685
Frame 2/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 188.057
Frame 3/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 205.016
Frame 4/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 220.705
Frame 5/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 240.041
Frame 6/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 265.049
Frame 7/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 293.517
Frame 8/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 318.584
Frame 9/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 331.283
Frame 10/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 329.358
Frame 11/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 344.78
Frame 12/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 356.991
Frame 13/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 359.497
Frame 14/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 362.797
Frame 15/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 377.997
Frame 16/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 381.348
Frame 17/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 389.148
Frame 18/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 411.827
Frame 19/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 436.811
Frame 20/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 488.973
Frame 21/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 526.659
Frame 22/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 555.669
Frame 23/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 561.79
Frame 24/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 580.393
Frame 25/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 599.201
Frame 26/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 1388.74
Frame 27/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 2671.78
Frame 28/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 697.212
Frame 29/30
launching 5625 blocks of 256 threads
Kernel = draw(int*, int*, int*, Vec*, Vec*)
grid X Y Z = 5625, 1, 1
block X Y Z = 256, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 256
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
kernel time: 775.599
Run time: 16.0669
Run times
16.0669
Kernel times
775.599
