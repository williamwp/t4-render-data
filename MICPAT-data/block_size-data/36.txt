------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
x,y,z: 0.000000, 0.000000, -20.000000, radius: 4.000000
x,y,z: 0.000000, 20.000000, -30.000000, radius: 3.000000
x,y,z: 0.000000, -10004.000000, -20.000000, radius: 10000.000000
x,y,z: 5.000000, -1.000000, -15.000000, radius: 2.000000
x,y,z: 5.000000, 0.000000, -25.000000, radius: 3.000000
x,y,z: -5.500000, 0.000000, -15.000000, radius: 3.000000
Kernel = tracer_kernel(float3*)
grid X Y Z = 64, 64, 1
block X Y Z = 8, 8, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 64
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
