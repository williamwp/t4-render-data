------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Input image size : 1920 x 1440
grainRadius : 0.05
sigmaR : 0
sigmaFilter : 0.8
NmonteCarlo : 800
colour
xA : 0
yA : 0
xB : 1920
yB : 1440
mOut : 1440
nOut : 1920
randomizeSeed : 1
***************************
image size : 1920 x 1440
------------------
muR : 0.05
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
blocks.x : 69
blocks.y : 52
blocks.z : 1
Kernel = kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)
grid X Y Z = 69, 52, 1
block X Y Z = 28, 28, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 784
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Time to generate:  1426.8 ms; Framerate: 0.70 images/sec
image size : 1920 x 1440
------------------
muR : 0.05
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
blocks.x : 69
blocks.y : 52
blocks.z : 1
Kernel = kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)
grid X Y Z = 69, 52, 1
block X Y Z = 28, 28, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 784
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Time to generate:  1165.7 ms; Framerate: 0.86 images/sec
image size : 1920 x 1440
------------------
muR : 0.05
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
blocks.x : 69
blocks.y : 52
blocks.z : 1
Kernel = kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)
grid X Y Z = 69, 52, 1
block X Y Z = 28, 28, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 784
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Time to generate:  1219.8 ms; Framerate: 0.82 images/sec
output file name : 22.png
time elapsed : 5.57298
***************************


