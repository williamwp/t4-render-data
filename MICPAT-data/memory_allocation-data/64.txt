wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs$ cd 65-Ray-tracing-with-materials-sample3/
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/65-Ray-tracing-with-materials-sample3$ ls
65-2080-ncu-130.txt                   65-p100-nsys-profile.txt                hitable.h       material.h
65-a100-ncu-130.txt                   65-p100-nvprof-160-backup1.txt          hitable_list.h  ray.h
65-a100-ncu-21-metrics-full.txt       65-t4-ncu-130-plus-21-metrics-full.txt  Image.ppm       read-test-demo.txt
65-a100-nsys-ncu-21-metrics-temp.txt  65-t4-nsys-profile.txt                  install.sh      sphere.h
65-a100-nsys-profile-full.txt         a.out                                   main.cu         vec3.h
65-a100-nsys-profile.txt              camera.h                                Makefile
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/65-Ray-tracing-with-materials-sample3$ CUDA_VISIBLE_DEVICES=1   LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/memory_allocation/memory_allocation.so  ./a.out 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Found an allocation!
0x7fb655600000, 3904 
Found an allocation!
0x7fb655601000, 8 
Found an allocation!
0x7fb655601200, 8 
Rendering Image: 100x100
Tempo de Execução: 810.483 segundos
Found a free!
0x7fb655601200
Address: 0x7fb655601000, Number: 1, Size: 8
Address: 0x7fb655600000, Number: 0, Size: 3904
Found a free!
0x7fb655601000
Address: 0x7fb655600000, Number: 0, Size: 3904
Found a free!
0x7fb655600000
Found a free!
0x7fb682200000
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/65-Ray-tracing-with-materials-sample3$ 
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/65-Ray-tracing-with-materials-sample3$ 
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/65-Ray-tracing-with-materials-sample3$ cd ..
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs$ cd 64-Film-grain-rendering-gpu-sample3/bin/
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/64-Film-grain-rendering-gpu-sample3/bin$ ls
22.png           4.png                                 64-a100-nsys-profile-full.txt      64-t4-nsys-profile.txt
2-li-bai.png     64-2080-ncu-130.txt                   64-a100-nsys-profile.txt           film_grain_rendering_main
2.png            64-a100-ncu-130.txt                   64-p100-nsys-profile.txt           li-bai-render.png
44.png           64-a100-ncu-21-metrics-full.txt       64-p100-nvprof-160-backup1.txt     read.txt
4-diao-chan.png  64-a100-nsys-ncu-21-metrics-temp.txt  64-t4-ncu-130-plus-21-metrics.txt
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/64-Film-grain-rendering-gpu-sample3/bin$ CUDA_VISIBLE_DEVICES=1   LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/memory_allocation/memory_allocation.so ./film_grain_rendering_main 2.png 22.png 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Input image size : 1920 x 1440
grainRadius : 0.2
sigmaR : 0
sigmaFilter : 0.8
NmonteCarlo : 800
colour
xA : 0
yA : 0
xB : 1920
yB : 1440
mOut : 1440
nOut : 1920
randomizeSeed : 1
***************************
image size : 1920 x 1440
------------------
muR : 0.2
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fa85b200000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  954567.8 ms; Framerate: 0.00 images/sec
Found a free!
0x7fa85b200000
image size : 1920 x 1440
------------------
muR : 0.2
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fa84a800000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  818469.8 ms; Framerate: 0.00 images/sec
Found a free!
0x7fa84a800000
image size : 1920 x 1440
------------------
muR : 0.2
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fa84a800000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  960366.3 ms; Framerate: 0.00 images/sec
Found a free!
0x7fa84a800000
output file name : 22.png
time elapsed : 2735.11
***************************

