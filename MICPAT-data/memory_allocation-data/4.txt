wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/4-Film-grain-rendering-gpu-sample1/bin$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/memory_allocation/memory_allocation.so  ./film_grain_rendering_main 2.png 22.png 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Input image size : 1920 x 1440
grainRadius : 0.1
sigmaR : 0
sigmaFilter : 0.8
NmonteCarlo : 800
colour
xA : 0
yA : 0
xB : 1920
yB : 1440
mOut : 1440
nOut : 1920
randomizeSeed : 1
***************************
image size : 1920 x 1440
------------------
muR : 0.1
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fe891200000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  1051206.6 ms; Framerate: 0.00 images/sec
Found a free!
0x7fe891200000
image size : 1920 x 1440
------------------
muR : 0.1
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fe880800000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  947174.9 ms; Framerate: 0.00 images/sec
Found a free!
0x7fe880800000
image size : 1920 x 1440
------------------
muR : 0.1
sigmaR : 0
zoom, s : 1
sigmaFilter : 0.8
NmonteCarlo : 800
xA : 0
yA : 0
xB : 1920
yB : 1440
randomizeSeed : 1
------------------
Output image size is 1920 x 1440
Found an allocation!
0x7fe880800000, 11059200 
blocks.x : 69
blocks.y : 52
blocks.z : 1
Time to generate:  981472.1 ms; Framerate: 0.00 images/sec
Found a free!
0x7fe880800000
output file name : 22.png
time elapsed : 2981.66
***************************

