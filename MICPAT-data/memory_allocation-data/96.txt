wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/96-game-of-life$ LD_PRELOAD=/home/wangpeng/tools/gitlab-william/cpu-gpu-raytracing/MICPAT-new/tools/memory_allocation/memory_allocation.so ./gui 
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Is there a configuration file? (y/n): n
Enter the number of rows in the grid: 100
Enter the number of columns in the grid: 100
Starting display
Initializing the grid
Finished initializing the grid
Initializing the texture
Finished initializing the texture
Starting render...
Switching compute mode...
Found an allocation!
0x7fdb33000000, 40000 
Found an allocation!
0x7fdb33009e00, 40000 
Found an allocation!
0x7fdb33013c00, 120000 
Encountered a memcpy HtoD!
0x7fdb33000000, 0x5651f2c0eb40, 40000 
Encountered a memcpy HtoD!
0x7fdb33000000, 0x5651f2c0eb40, 40000 
Encountered a memcpy HtoD!
0x7fdb33009e00, 0x5651f2c18790, 40000 
Encountered a memcpy HtoD!
0x7fdb33009e00, 0x5651f2c18790, 40000 
Encountered a memcpy HtoD!
0x7fdb33013c00, 0x5651f2c223e0, 120000 
Encountered a memcpy HtoD!
0x7fdb33013c00, 0x5651f2c223e0, 120000 
Compute mode set to GPU

