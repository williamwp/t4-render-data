==PROF== Connected to process 69149 (/home/wangpeng/t4-render-data/150-rendering-data/137-RayTracing_Parallel/build/RayTracing_cuda)
Rendering scene:
Width: 2000 
Height: 2000
Fov: 20.00
Blocks: 126 x 126
==PROF== Profiling "k_trace" - 1: 0%....50%....100% - 13 passes
100.00%
Finished!
Rendering time: 1.605 s
==PROF== Disconnected from process 69149
[69149] RayTracing_cuda@127.0.0.1
  k_trace(Color*, Plane*, int, Sphere*, int, Light*, int, float, float, int, int), 2023-Nov-29 00:34:55, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Kbyte                          39.17
    dram__bytes_read.sum.per_second                                           Mbyte/second                          14.34
    dram__bytes_write.sum                                                            Mbyte                         344.65
    dram__bytes_write.sum.per_second                                          Gbyte/second                         126.20
    dram__sectors_read.sum                                                          sector                          1,224
    dram__sectors_write.sum                                                         sector                     10,770,196
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          26.75
    l1tex__lsu_writeback_active.sum                                                  cycle                     58,506,859
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                         400.96
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                            256
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Gbyte/second                          93.74
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                         138.26
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                     34,218,823
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                        375,000
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                      3,290,152
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                      9,419,536
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          84.13
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                     34,218,823
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                     11,799,584
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                     36,307,703
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/nsecond                          13.29
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                         112.86
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                          13.90
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          99.98
    lts__t_sector_op_read_hit_rate.pct                                                   %                          24.27
    lts__t_sector_op_write_hit_rate.pct                                                  %                          99.98
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                          17.95
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              7
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                           2.56
    lts__t_sectors_op_read.sum                                                      sector                         18,214
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                           6.67
    lts__t_sectors_op_write.sum                                                     sector                     58,896,681
    lts__t_sectors_op_write.sum.per_second                                  sector/nsecond                          21.57
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                          4,061
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                           1.49
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                          13.22
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          34.89
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                       4,577.39
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          96.28
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.67
    smsp__inst_executed.sum                                                           inst                    581,364,922
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                     34,218,823
    smsp__inst_executed_op_global_st.sum                                              inst                        375,000
    smsp__inst_executed_op_local_ld.sum                                               inst                        822,538
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                        504,016
    smsp__inst_executed_pipe_cbu.sum                                                  inst                     55,356,010
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                          11.16
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           4.52
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.67
    smsp__inst_issued.sum                                                             inst                    581,659,144
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          66.74
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                  1,644,411,908
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                    201,131,829
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                    273,094,265
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                     59,852,539
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                    761,645,169
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                  2,134,652,124
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                    765,944,164
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                  5,916,416,332
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                    534,817,517
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                  1,881,971,313
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                    955,096,255
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                  1,407,450,002
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          72.32
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              23.14
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.04
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                         628.71
    ---------------------------------------------------------------------- --------------- ------------------------------

