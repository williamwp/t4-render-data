==15247== Replaying kernel "initChildNode(DOctTree**, int, DOctTree*)" (1 of 59)... 
	4 internal events
^C==15247== Profiling application: ./run -s scene3.json
==15247== Profiling result:
==15247== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: initChildNode(DOctTree**, int, DOctTree*)
       1650                             inst_per_warp                                                 Instructions per warp   43.000000   43.000000   43.000000
       1650                         branch_efficiency                                                     Branch Efficiency     100.00%     100.00%     100.00%
       1650                 warp_execution_efficiency                                             Warp Execution Efficiency       3.05%       3.20%       3.12%
       1650         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency       3.05%       3.20%       3.12%
       1650                      inst_replay_overhead                                           Instruction Replay Overhead    0.023256    0.023256    0.023256
       1650      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
       1650     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
       1650       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
       1650      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
       1650              gld_transactions_per_request                                  Global Load Transactions Per Request    0.000000    0.000000    0.000000
       1650              gst_transactions_per_request                                 Global Store Transactions Per Request    1.000000    1.000000    1.000000
       1650                 shared_store_transactions                                             Shared Store Transactions           0           0           0
       1650                  shared_load_transactions                                              Shared Load Transactions           0           0           0
       1650                   local_load_transactions                                               Local Load Transactions           0           0           0
       1650                  local_store_transactions                                              Local Store Transactions           0           0           0
       1650                          gld_transactions                                              Global Load Transactions           2           2           2
       1650                          gst_transactions                                             Global Store Transactions           1           1           1
       1650                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
       1650                 sysmem_write_transactions                                      System Memory Write Transactions           4           4           4
       1650                      l2_read_transactions                                                  L2 Read Transactions          17          89          26
       1650                     l2_write_transactions                                                 L2 Write Transactions          14          14          14
       1650                    dram_read_transactions                                       Device Memory Read Transactions           0          28           0
       1650                   dram_write_transactions                                      Device Memory Write Transactions           0          20           0
       1650                           global_hit_rate                                     Global Hit Rate in unified l1/tex       0.00%       0.00%       0.00%
       1650                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
       1650                  gld_requested_throughput                                      Requested Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                  gst_requested_throughput                                     Requested Global Store Throughput  2.5364MB/s  2.6676MB/s  2.6308MB/s
       1650                            gld_throughput                                                Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                            gst_throughput                                               Global Store Throughput  10.145MB/s  10.670MB/s  10.524MB/s
       1650                     local_memory_overhead                                                 Local Memory Overhead       0.00%       0.00%       0.00%
       1650                        tex_cache_hit_rate                                                Unified Cache Hit Rate       0.00%       0.00%       0.00%
       1650                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)       0.00%       0.00%       0.00%
       1650                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)       0.00%     100.00%      98.12%
       1650                      dram_read_throughput                                         Device Memory Read Throughput  0.00000B/s  294.75MB/s  8.3621MB/s
       1650                     dram_write_throughput                                        Device Memory Write Throughput  0.00000B/s  211.63MB/s  8.6172MB/s
       1650                      tex_cache_throughput                                              Unified Cache Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  0.00000B/s  0.00000B/s  0.00000B/s
       1650                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  10.145MB/s  10.670MB/s  10.524MB/s
       1650                        l2_read_throughput                                                 L2 Throughput (Reads)  172.47MB/s  906.56MB/s  283.22MB/s
       1650                       l2_write_throughput                                                L2 Throughput (Writes)  142.04MB/s  149.39MB/s  147.34MB/s
       1650                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                   sysmem_write_throughput                                        System Memory Write Throughput  40.582MB/s  42.682MB/s  42.097MB/s
       1650                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
       1650                            gld_efficiency                                         Global Memory Load Efficiency       0.00%       0.00%       0.00%
       1650                            gst_efficiency                                        Global Memory Store Efficiency      25.00%      25.00%      25.00%
       1650                    tex_cache_transactions                                            Unified Cache Transactions           0           0           0
       1650                             flop_count_dp                           Floating Point Operations(Double Precision)           0           0           0
       1650                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)           0           0           0
       1650                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)           0           0           0
       1650                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)           0           0           0
       1650                             flop_count_sp                           Floating Point Operations(Single Precision)           0           0           0
       1650                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)           0           0           0
       1650                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)           0           0           0
       1650                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)           0           0           0
       1650                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)           0           0           0
       1650                             inst_executed                                                 Instructions Executed          43          43          43
       1650                               inst_issued                                                   Instructions Issued          44          44          44
       1650                          dram_utilization                                             Device Memory Utilization    Idle (0)     Low (1)    Idle (0)
       1650                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
       1650                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)       1.23%      40.33%       6.79%
       1650                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      47.12%      84.51%      73.39%
       1650                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       0.00%       0.00%       0.00%
       1650                             stall_texture                                         Issue Stall Reasons (Texture)       0.00%       0.00%       0.00%
       1650                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
       1650                               stall_other                                           Issue Stall Reasons (Other)       0.43%       6.98%       0.93%
       1650          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)      12.01%      28.40%      18.72%
       1650                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       0.00%       0.00%       0.00%
       1650                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
       1650                                inst_fp_32                                               FP Instructions(Single)           0           0           0
       1650                                inst_fp_64                                               FP Instructions(Double)           0           0           0
       1650                              inst_integer                                                  Integer Instructions           7           7           7
       1650                          inst_bit_convert                                              Bit-Convert Instructions           1           1           1
       1650                              inst_control                                             Control-Flow Instructions           1           1           1
       1650                        inst_compute_ld_st                                               Load/Store Instructions           4           4           4
       1650                                 inst_misc                                                     Misc Instructions          30          30          30
       1650           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
       1650                               issue_slots                                                           Issue Slots          44          44          44
       1650                                 cf_issued                                      Issued Control-Flow Instructions           4           4           4
       1650                               cf_executed                                    Executed Control-Flow Instructions           4           4           4
       1650                               ldst_issued                                        Issued Load/Store Instructions           4           4           4
       1650                             ldst_executed                                      Executed Load/Store Instructions           4           4           4
       1650                       atomic_transactions                                                   Atomic Transactions           0           0           0
       1650           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
       1650                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
       1650                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
       1650                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)           0           0           0
       1650                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.27%       0.16%
       1650                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       0.00%       0.00%       0.00%
       1650                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)           1           1           1
       1650                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
       1650                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
       1650                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
       1650                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
       1650                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
       1650                                       ipc                                                          Executed IPC    0.018968    0.036878    0.034440
       1650                                issued_ipc                                                            Issued IPC    0.021548    0.037736    0.035308
       1650                    issue_slot_utilization                                                Issue Slot Utilization       1.08%       1.89%       1.77%
       1650                             sm_efficiency                                               Multiprocessor Activity       0.36%       0.59%       0.43%
       1650                        achieved_occupancy                                                    Achieved Occupancy    0.015625    0.015625    0.015625
       1650                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    0.019749    0.037736    0.035386
       1650                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
       1650                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
       1650                           tex_utilization                                             Unified Cache Utilization    Idle (0)    Idle (0)    Idle (0)
       1650                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
       1650                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (1)     Low (1)     Low (1)
       1650                        tex_fu_utilization                                     Texture Function Unit Utilization     Low (1)     Low (1)     Low (1)
       1650                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
       1650             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
       1650           single_precision_fu_utilization                            Single-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
       1650           double_precision_fu_utilization                            Double-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
       1650                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
       1650                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       0.00%       0.00%       0.00%
       1650                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.00%       0.00%       0.00%
       1650                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
       1650                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
       1650               pcie_total_data_transmitted                                           PCIe Total Data Transmitted           0        1024         206
       1650                  pcie_total_data_received                                              PCIe Total Data Received           0         512          69
       1650                inst_executed_global_loads                              Warp level instructions for global loads           0           0           0
       1650                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
       1650                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
       1650               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
       1650               inst_executed_global_stores                             Warp level instructions for global stores           1           1           1
       1650                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
       1650               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
       1650              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
       1650              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
       1650           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
       1650             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
       1650          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
       1650              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
       1650                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
       1650                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads           0           0           0
       1650                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
       1650                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
       1650                           dram_read_bytes                                Total bytes read from DRAM to L2 cache           0         896          25
       1650                          dram_write_bytes                             Total bytes written from L2 cache to DRAM           0         640          26
       1650               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.          32          32          32
       1650                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
       1650              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
       1650                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
       1650                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
       1650             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
       1650                      global_load_requests              Total number of global load requests from Multiprocessor           0           0           0
       1650                       local_load_requests               Total number of local load requests from Multiprocessor           0           0           0
       1650                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
       1650                     global_store_requests             Total number of global store requests from Multiprocessor           1           1           1
       1650                      local_store_requests              Total number of local store requests from Multiprocessor           0           0           0
       1650                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
       1650                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
       1650                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
       1650                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
       1650                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
       1650                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
       1650                        sysmem_write_bytes                                             System Memory Write Bytes         128         128         128
       1650                           l2_tex_hit_rate                                                     L2 Cache Hit Rate       0.00%     100.00%      98.12%
       1650                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
       1650                     unique_warps_launched                                              Number of warps launched           1           1           1
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/15-3D-stanford-bunny-sample1$ 
