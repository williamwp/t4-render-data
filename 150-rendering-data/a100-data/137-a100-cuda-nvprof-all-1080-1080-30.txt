Height: 1000
Fov: 30.00
Blocks: 63 x 63
==PROF== Profiling "k_trace" - 1: 0%....50%....100% - 13 passes
100.00%
Finished!
Rendering time: 1.542 s
==PROF== Disconnected from process 99106
[99106] RayTracing_cuda@127.0.0.1
  k_trace(Color*, Plane*, int, Sphere*, int, Light*, int, float, float, int, int), 2023-Nov-28 20:56:48, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Kbyte                          17.15
    dram__bytes_read.sum.per_second                                           Mbyte/second                          18.43
    dram__bytes_write.sum                                                            Mbyte                          89.70
    dram__bytes_write.sum.per_second                                          Gbyte/second                          96.39
    dram__sectors_read.sum                                                          sector                            536
    dram__sectors_write.sum                                                         sector                      2,803,200
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          24.61
    l1tex__lsu_writeback_active.sum                                                  cycle                     17,363,792
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                         355.17
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                             64
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Gbyte/second                          68.77
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                    Gbyte/second                         111.09
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                     10,329,416
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                         93,768
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                        955,632
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                      2,488,312
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          83.98
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                     10,329,416
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                      3,230,776
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                      9,351,153
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                sector/nsecond                          10.05
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                          75.07
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                           3.53
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          99.97
    lts__t_sector_op_read_hit_rate.pct                                                   %                          18.93
    lts__t_sector_op_write_hit_rate.pct                                                  %                          99.98
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                          13.86
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              8
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                           8.60
    lts__t_sectors_op_read.sum                                                      sector                         14,197
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                          15.25
    lts__t_sectors_op_write.sum                                                     sector                     15,150,055
    lts__t_sectors_op_write.sum.per_second                                  sector/nsecond                          16.28
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                          2,521
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                           2.71
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                          11.63
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          33.05
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                       5,639.46
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          90.32
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.64
    smsp__inst_executed.sum                                                           inst                    179,064,189
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                     10,329,416
    smsp__inst_executed_op_global_st.sum                                              inst                         93,768
    smsp__inst_executed_op_local_ld.sum                                               inst                        238,908
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                        126,016
    smsp__inst_executed_pipe_cbu.sum                                                  inst                     17,653,167
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                          10.82
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           4.20
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.64
    smsp__inst_issued.sum                                                             inst                    179,229,298
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          64.50
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                    412,232,265
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                     50,762,527
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                     68,768,959
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                     15,128,553
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                    191,016,851
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                    531,434,077
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                    190,228,401
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                  1,474,993,230
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                    134,925,599
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                    471,913,402
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                    238,220,407
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                    352,890,888
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          58.73
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              18.79
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.12
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                         582.28
    ---------------------------------------------------------------------- --------------- ------------------------------

