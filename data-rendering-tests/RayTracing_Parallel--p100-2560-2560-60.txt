    Kernel: k_trace(Color*, Plane*, int, Sphere*, int, Light*, int, float, float, int, int)
          1                             inst_per_warp                                                 Instructions per warp  4.3606e+03  4.3606e+03  4.3606e+03
          1                         branch_efficiency                                                     Branch Efficiency      98.02%      98.02%      98.02%
          1                 warp_execution_efficiency                                             Warp Execution Efficiency      75.60%      75.60%      75.60%
          1         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      72.43%      72.43%      72.43%
          1                      inst_replay_overhead                                           Instruction Replay Overhead    0.000067    0.000067    0.000067
          1      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
          1     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
          1       local_load_transactions_per_request                            Local Memory Load Transactions Per Request   27.708041   27.708041   27.708041
          1      local_store_transactions_per_request                           Local Memory Store Transactions Per Request   10.532731   10.532731   10.532731
          1              gld_transactions_per_request                                  Global Load Transactions Per Request   23.510366   23.510366   23.510366
          1              gst_transactions_per_request                                 Global Store Transactions Per Request   32.000000   32.000000   32.000000
          1                 shared_store_transactions                                             Shared Store Transactions           0           0           0
          1                  shared_load_transactions                                              Shared Load Transactions           0           0           0
          1                   local_load_transactions                                               Local Load Transactions    35576792    35576792    35576792
          1                  local_store_transactions                                              Local Store Transactions    58836912    58836912    58836912
          1                          gld_transactions                                              Global Load Transactions  1246927176  1246927176  1246927176
          1                          gst_transactions                                             Global Store Transactions    19660800    19660800    19660800
          1                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
          1                 sysmem_write_transactions                                      System Memory Write Transactions           5           5           5
          1                      l2_read_transactions                                                  L2 Read Transactions    19045977    19045977    19045977
          1                     l2_write_transactions                                                 L2 Write Transactions    79848935    79848935    79848935
          1                    dram_read_transactions                                       Device Memory Read Transactions     1697387     1697387     1697387
          1                   dram_write_transactions                                      Device Memory Write Transactions    60124916    60124916    60124916
          1                           global_hit_rate                                     Global Hit Rate in unified l1/tex      99.33%      99.33%      99.33%
          1                            local_hit_rate                                                        Local Hit Rate      47.46%      47.46%      47.46%
          1                  gld_requested_throughput                                      Requested Global Load Throughput  32.882GB/s  32.882GB/s  32.882GB/s
          1                  gst_requested_throughput                                     Requested Global Store Throughput  7.5518GB/s  7.5518GB/s  7.5518GB/s
          1                            gld_throughput                                                Global Load Throughput  162.97GB/s  162.97GB/s  162.97GB/s
          1                            gst_throughput                                               Global Store Throughput  60.414GB/s  60.414GB/s  60.414GB/s
          1                     local_memory_overhead                                                 Local Memory Overhead      79.48%      79.48%      79.48%
          1                        tex_cache_hit_rate                                                Unified Cache Hit Rate      73.18%      73.18%      73.18%
          1                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      93.13%      93.13%      93.13%
          1                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      37.45%      37.45%      37.45%
          1                      dram_read_throughput                                         Device Memory Read Throughput  5.2158GB/s  5.2158GB/s  5.2158GB/s
          1                     dram_write_throughput                                        Device Memory Write Throughput  184.75GB/s  184.75GB/s  184.75GB/s
          1                      tex_cache_throughput                                              Unified Cache Throughput  1021.1GB/s  1021.1GB/s  1021.1GB/s
          1                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  58.520GB/s  58.520GB/s  58.520GB/s
          1                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  241.21GB/s  241.21GB/s  241.21GB/s
          1                        l2_read_throughput                                                 L2 Throughput (Reads)  58.525GB/s  58.525GB/s  58.525GB/s
          1                       l2_write_throughput                                                L2 Throughput (Writes)  245.36GB/s  245.36GB/s  245.36GB/s
          1                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   sysmem_write_throughput                                        System Memory Write Throughput  16.110KB/s  16.110KB/s  16.109KB/s
          1                     local_load_throughput                                          Local Memory Load Throughput  109.32GB/s  109.32GB/s  109.32GB/s
          1                    local_store_throughput                                         Local Memory Store Throughput  180.80GB/s  180.80GB/s  180.80GB/s
          1                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          1                            gld_efficiency                                         Global Memory Load Efficiency      20.18%      20.18%      20.18%
          1                            gst_efficiency                                        Global Memory Store Efficiency      12.50%      12.50%      12.50%
          1                    tex_cache_transactions                                            Unified Cache Transactions   332311440   332311440   332311440
          1                             flop_count_dp                           Floating Point Operations(Double Precision)  1295164668  1295164668  1295164668
          1                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)   322428607   322428607   322428607
          1                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)   437849616   437849616   437849616
          1                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)    97036829    97036829    97036829
          1                             flop_count_sp                           Floating Point Operations(Single Precision)  9254956397  9254956397  9254956397
          1                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)  1215038293  1215038293  1215038293
          1                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  3405627687  3405627687  3405627687
          1                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)  1228662730  1228662730  1228662730
          1                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   322927777   322927777   322927777
          1                             inst_executed                                                 Instructions Executed   904252101   904252101   904252101
          1                               inst_issued                                                   Instructions Issued   904312213   904312213   904312213
          1                          dram_utilization                                             Device Memory Utilization     Low (3)     Low (3)     Low (3)
          1                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
          1                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)      27.74%      27.74%      27.74%
          1                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      17.15%      17.15%      17.15%
          1                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)      15.62%      15.62%      15.62%
          1                             stall_texture                                         Issue Stall Reasons (Texture)       1.89%       1.89%       1.89%
          1                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
          1                               stall_other                                           Issue Stall Reasons (Other)      31.20%      31.20%      31.20%
          1          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.01%       0.01%       0.01%
          1                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       2.15%       2.15%       2.15%
          1                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
          1                                inst_fp_32                                               FP Instructions(Single)  7521711982  7521711982  7521711982
          1                                inst_fp_64                                               FP Instructions(Double)   859137051   859137051   859137051
          1                              inst_integer                                                  Integer Instructions  2584601799  2584601799  2584601799
          1                          inst_bit_convert                                              Bit-Convert Instructions   153028931   153028931   153028931
          1                              inst_control                                             Control-Flow Instructions  1882616901  1882616901  1882616901
          1                        inst_compute_ld_st                                               Load/Store Instructions  1536512671  1536512671  1536512671
          1                                 inst_misc                                                     Misc Instructions  6419872828  6419872828  6419872828
          1           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
          1                               issue_slots                                                           Issue Slots   808395457   808395457   808395457
          1                                 cf_issued                                      Issued Control-Flow Instructions   112149871   112149871   112149871
          1                               cf_executed                                    Executed Control-Flow Instructions   112149871   112149871   112149871
          1                               ldst_issued                                        Issued Load/Store Instructions   274987263   274987263   274987263
          1                             ldst_executed                                      Executed Load/Store Instructions    62170497    62170497    62170497
          1                       atomic_transactions                                                   Atomic Transactions           0           0           0
          1           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
          1                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
          1                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
          1                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)    19044562    19044562    19044562
          1                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.62%       0.62%       0.62%
          1                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       3.62%       3.62%       3.62%
          1                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)    78497712    78497712    78497712
          1                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
          1                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
          1                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
          1                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
          1                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
          1                                       ipc                                                          Executed IPC    1.266769    1.266769    1.266769
          1                                issued_ipc                                                            Issued IPC    1.265473    1.265473    1.265473
          1                    issue_slot_utilization                                                Issue Slot Utilization      56.56%      56.56%      56.56%
          1                             sm_efficiency                                               Multiprocessor Activity      99.21%      99.21%      99.21%
          1                        achieved_occupancy                                                    Achieved Occupancy    0.350404    0.350404    0.350404
          1                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    1.895800    1.895800    1.895800
          1                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
          1                            l2_utilization                                                  L2 Cache Utilization     Low (2)     Low (2)     Low (2)
          1                           tex_utilization                                             Unified Cache Utilization     Mid (5)     Mid (5)     Mid (5)
          1                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
          1                        tex_fu_utilization                                     Texture Function Unit Utilization     Mid (4)     Mid (4)     Mid (4)
          1                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
          1           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Mid (5)     Mid (5)     Mid (5)
          1           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
          1                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
          1                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)      10.05%      10.05%      10.05%
          1                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       2.81%       2.81%       2.81%
          1                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
          1                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
          1               pcie_total_data_transmitted                                           PCIe Total Data Transmitted        6656        6656        6656
          1                  pcie_total_data_received                                              PCIe Total Data Received           0           0           0
          1                inst_executed_global_loads                              Warp level instructions for global loads    53037335    53037335    53037335
          1                 inst_executed_local_loads                               Warp level instructions for local loads     1283988     1283988     1283988
          1                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
          1               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
          1               inst_executed_global_stores                             Warp level instructions for global stores      614400      614400      614400
          1                inst_executed_local_stores                              Warp level instructions for local stores     5586102     5586102     5586102
          1               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
          1              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
          1              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
          1           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
          1             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
          1          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
          1              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
          1                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
          1                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads    11367616    11367616    11367616
          1                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads   598097152   598097152   598097152
          1                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
          1                           dram_read_bytes                                Total bytes read from DRAM to L2 cache    54316384    54316384    54316384
          1                          dram_write_bytes                             Total bytes written from L2 cache to DRAM  1923997312  1923997312  1923997312
          1               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.  2511926784  2511926784  2511926784
          1                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
          1              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
          1                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
          1                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
          1             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
          1                      global_load_requests              Total number of global load requests from Multiprocessor   192571223   192571223   192571223
          1                       local_load_requests               Total number of local load requests from Multiprocessor    19081688    19081688    19081688
          1                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
          1                     global_store_requests             Total number of global store requests from Multiprocessor     2457600     2457600     2457600
          1                      local_store_requests              Total number of local store requests from Multiprocessor    59228080    59228080    59228080
          1                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
          1                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
          1                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
          1                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
          1                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
          1                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
          1                        sysmem_write_bytes                                             System Memory Write Bytes         160         160         160
          1                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      48.32%      48.32%      48.32%
          1                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
          1                     unique_warps_launched                                              Number of warps launched      207368      207368      207368

