wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/69-Cuda-mandel3d-sample3$ sudo nvprof --events  shared_ld_bank_conflict,shared_st_bank_conflict  ./a.out 
==21328== NVPROF is profiling process 21328, command: ./a.out
preparing depth cache ... done
ray marching ... done
building depth cache ... done
depth cache size:344128
...............
==21328== Profiling application: ./a.out
==21328== Profiling result:
==21328== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: AXPY_cukern(float*, int, float, float)
          1                   shared_ld_bank_conflict           0           0           0           0
          1                   shared_st_bank_conflict           0           0           0           0
    Kernel: void shaderNormal_cukern<RenderActionType=2, CubeSquareFractal, IndexedRayLookup, DefaultEnvMap>(vec4*, IndexedRayLookup, DefaultEnvMap, CubeSquareFractal, int)
         15                   shared_ld_bank_conflict           0           0           0           0
         15                   shared_st_bank_conflict           0           0           0           0
    Kernel: renderDepthCache_cukern(RayPtr*, int*, Ray const *, int)
          1                   shared_ld_bank_conflict           0           0           0           0
          1                   shared_st_bank_conflict           0           0           0           0
    Kernel: void initRayBuffer_cukern<bool=0>(Ray*, int, int, int, mat4x4)
          1                   shared_ld_bank_conflict           0           0           0           0
          1                   shared_st_bank_conflict           0           0           0           0
    Kernel: void rtRaymarch_cukern<CubeSquareFractal, bool=1>(Ray*, int, int, CubeSquareFractal, int)
         16                   shared_ld_bank_conflict           0           0           0           0
         16                   shared_st_bank_conflict           0           0           0           0
    Kernel: void initRayBufferWithDepthCache_cukern<bool=1>(Ray*, int, int, RayPtr*, int, int, mat4x4)
         15                   shared_ld_bank_conflict           0           0           0           0
         15                   shared_st_bank_conflict           0           0           0           0

