-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: systemback
Binary: systemback, systemback-dbg, systemback-cli, systemback-cli-dbg, systemback-scheduler, systemback-scheduler-dbg, libsystemback, libsystemback-dbg, systemback-efiboot-amd64, systemback-locales
Architecture: amd64 i386 all
Version: 1.8.5
Maintainer: Kende Krisztián <nemh@freemail.hu>, Zhang Weijia (randoms) <randoms@bwbot.org>
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9), libblkid-dev, libmount-dev, libncursesw5-dev, libparted-dev, qtbase5-dev, qttools5-dev-tools
Package-List:
 libsystemback deb libs optional arch=amd64,i386
 libsystemback-dbg deb debug extra arch=amd64,i386
 systemback deb admin optional arch=amd64,i386
 systemback-cli deb admin optional arch=amd64,i386
 systemback-cli-dbg deb debug extra arch=amd64,i386
 systemback-dbg deb debug extra arch=amd64,i386
 systemback-efiboot-amd64 deb admin optional arch=all
 systemback-locales deb admin optional arch=all
 systemback-scheduler deb admin optional arch=amd64,i386
 systemback-scheduler-dbg deb debug extra arch=amd64,i386
Checksums-Sha1:
 c5da603a9593e342ea113d2d4a20bc324c6dffd9 1754708 systemback_1.8.5.tar.xz
Checksums-Sha256:
 c61a4c8df1899ba13df142632fa8781f4380280ccacd8ec8844048267524c0d4 1754708 systemback_1.8.5.tar.xz
Files:
 fe3a79a18bc6c7cf4e4920b286f7ae8b 1754708 systemback_1.8.5.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJe3uzaAAoJEK/aNax9nCefKk0H/2TDtG+NWCJFzqRBJqMI/qBx
BV+5vFrLPv1/lrn4eMZqx4MsMkDxkwyTI+WqxI+hQpWrPIP3Jbv6zLhClPCVL4wg
8lyDjD4LYrsFeDGyMcpJN6jI8WGjZKPbtKFeX1T8/HL2pNRG8OsbmN7AJn+EbFnl
tulmnjmD+lxF3605ADzr9893yvEijx4Jmq4jvsS6jXlPk8KEQ2gAyJDtsEzyh7L6
GUZiIgez7Y5Dx0GfVrsVHyv9fueMpK0QW7/yvPYYCjz2eEIKIclgl9PS6ewHFxOE
NUS8g4bN0ReB5uK6V8NTAlzufSXROE2ihCoLTbaUkVvoib3ux3EYr8g2YtPFjCQ=
=PJ2b
-----END PGP SIGNATURE-----
