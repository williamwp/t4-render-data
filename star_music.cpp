#include <alsa/asoundlib.h>
#include <iostream>
#include <unistd.h>
#include <cmath>

using namespace std;
// 音符频率（Hz）

const int C4 = 261;const int D4 = 294;const int E4 = 329;const int F4 = 349;const int G4 = 392;const int A4 = 440;const int B4 = 493;const int C5 = 523;
// 音符持续时间（微秒）
const int quarterNote = 500000;const int halfNote = 2 * quarterNote;const int wholeNote = 2 * halfNote;
// 播放音符函数

void playNote(snd_pcm_t *handle, int frequency, int duration) {
int sampleRate = 44100; // 采样率
int numSamples = (sampleRate * duration) / 1000000;
float *buffer = new float[numSamples];
for (int i = 0; i < numSamples; i++) {
float t = static_cast<float>(i) / sampleRate;
buffer[i] = sin(2 * M_PI * frequency * t);
}
snd_pcm_writei(handle, buffer, numSamples);
snd_pcm_drain(handle);
delete[] buffer;
}
int main() {
snd_pcm_t *handle;
snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
snd_pcm_set_params(handle, SND_PCM_FORMAT_FLOAT_LE, SND_PCM_ACCESS_RW_INTERLEAVED, 1, 44100, 1, 500000);
int melody[] = {C4, C4, G4, G4, A4, A4, G4, F4, F4, E4, E4, D4, D4, C4};
int rhythm[] = {quarterNote, quarterNote, quarterNote, quarterNote, quarterNote, quarterNote, halfNote,
quarterNote, quarterNote, quarterNote, quarterNote, quarterNote, quarterNote, halfNote};
for (int i = 0; i < sizeof(melody) / sizeof(melody[0]); i++) {
playNote(handle, melody[i], rhythm[i]);
usleep(100000); // 等待一段时间，以创建音符之间的间隔
}
snd_pcm_close(handle);
return 0;
}
